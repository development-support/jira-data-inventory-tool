VERSION 5.00
Begin {C62A69F0-16DC-11CE-9E98-00AA00574A4F} Info 
   Caption         =   "Processing Status"
   ClientHeight    =   3045
   ClientLeft      =   120
   ClientTop       =   435
   ClientWidth     =   4560
   OleObjectBlob   =   "Info.frx":0000
   StartUpPosition =   1  'オーナー フォームの中央
End
Attribute VB_Name = "Info"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = False
Option Explicit

Private Sub UserForm_Initialize()

  With ProgressBar1
    .Min = 0
    .Max = 100
    .Value = 0
  End With

  Percent.Caption = ""
 
End Sub
