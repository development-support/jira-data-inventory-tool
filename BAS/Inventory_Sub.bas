Attribute VB_Name = "Inventory_Sub"
Option Explicit

' 指定シートにおいて、指定文字列(見出し)のセル列を特定するFunction
' [引数]
'   strSheetName As String：検索対象シート名
'   strHead As String     ：見出し文字列
'   rowNum As Long        ：行番号を格納する変数
'   colNum As Long        ：列番号を格納する変数
'
' [戻り値]
'   Boolean：True  (指定文字列の見出しセル列取得に成功)
'          ：False (指定文字列の見出しセル列取得に失敗)
Private Function GetHeadPos(ByVal strSheetName As String, _
                            ByVal strHead As String, _
                            ByRef rowNum As Long, _
                            ByRef colNum As Long) As Boolean
  Dim wsRtn As Worksheet
  Dim FoundCell As Range
  Dim i As Long

  Set wsRtn = ActiveSheet
  ' 検索対象シートに切替
  Worksheets(strSheetName).Activate

  Set FoundCell = Cells.Find(What:=strHead, LookAt:=xlWhole)
  If FoundCell Is Nothing Then
    rowNum = 0
    colNum = 0
    Debug.Print "(GetHeadPos):strSheetName = " & strSheetName & _
                " strHead = " & strHead & " rowNum = " & rowNum & _
                " colNum = " & colNum
    GetHeadPos = False
    ' 元のシートに切替
    wsRtn.Activate
    Exit Function
  End If

  rowNum = FoundCell.Row
  colNum = FoundCell.Column

  ' 元のシートに切替
  wsRtn.Activate

  GetHeadPos = True

End Function

' 指定シートが存在するかチェックするFunction
' [引数]
'   strSheetName As String：シート名
'
' [戻り値]
'   Boolean：True  (指定シートが存在する)
'          ：False (指定シートが存在しない)
Public Function isSheetExist(ByVal strSheetName As String) As Boolean
  Dim ws As Worksheet
  Dim FindFlg As Boolean

  FindFlg = False

  For Each ws In Worksheets
    If ws.Name = strSheetName Then
      FindFlg = True
      Exit For
    End If
  Next ws

  If FindFlg = False Then
    isSheetExist = False
    Exit Function
  End If

  isSheetExist = True

End Function

' 設定パラメータ(FID情報)を取得するFunction
' [引数]
'   なし
'
' [戻り値]
'   なし
Public Function GetModeExclude()

  With Worksheets(gMenuSheet)
    ' Exclude設定値を取得
    gSettingFID.ExKey = .Range("ExKey").Value
    gSettingFID.ExField = .Range("ExField").Value
    gSettingFID.ExStr = .Range("ExString").Value
  End With

  Debug.Print "(GetModeExclude):gSettingFID.ExKey = " & gSettingFID.ExKey & _
              " gSettingFID.ExField = " & gSettingFID.ExField & _
              " gSettingFID.ExStr = " & gSettingFID.ExStr

End Function

' JIRA問処シートから、見出し(Key)に指定文字列が含まれる問処かつ
' 指定見出しに指定キーワード(文字列)が存在する問処を削除するFunction
' [引数]
'   strSheet As String：JIRA問処Dataが書かれたシート名
'   strContainKey As String：JIRA問処Dataの見出し名(Key)に含まれる文字列
'   strHeading As String：JIRA問処Dataの対象見出し名
'   strKeyword As String：JIRA問処Dataの対象見出しに含まれる、削除対象
'                         としたいキーワード(文字列)
'                         [カンマ区切りの複数Keywordに対応]
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function DeleteIssuesByKeyword(ByVal strSheet As String, _
                                      ByVal strContainKey As String, _
                                      ByVal strHeading As String, _
                                      ByVal strKeyword As String) As Boolean
  Const START_ROW As Long = 2
  Dim i As Long
  Dim j As Long
  Dim rowKey As Long
  Dim colKey As Long
  Dim rowHead As Long
  Dim colHead As Long
  Dim LastRow As Long
  Dim vKey As Variant
  Dim strCurKey As String

  Debug.Print "(DeleteIssuesByKeyword):strSheet = " & strSheet & _
              " strContainKey = " & strContainKey & _
              " strHeading = " & strHeading & _
              " strKeyword = " & strKeyword

  ' 見出し名(Key)の行/列番号を取得
  If (GetHeadPos(strSheet, gKeyHead, rowKey, colKey) = False) Then
    DeleteIssuesByKeyword = False
    Exit Function
  End If

  Debug.Print "(DeleteIssuesByKeyword):rowKey = " & rowKey & _
              " colKey = " & colKey

  ' 指定見出し名の行/列番号を取得
  If (GetHeadPos(strSheet, strHeading, rowHead, colHead) = False) Then
    DeleteIssuesByKeyword = False
    Exit Function
  End If

  Debug.Print "(DeleteIssuesByKeyword):rowHead = " & rowHead & _
              " colHead = " & colHead

  ' Keywordをカンマ区切りで分ける
  vKey = Split(strKeyword, ",")

  ' カンマ区切りで無い場合
  If (LBound(vKey) = UBound(vKey)) Then
    ' 見出し名(Key)列の最終行を取得
    LastRow = Cells(Rows.Count, colKey).End(xlUp).Row

    For i = LastRow To START_ROW Step -1
      ' 見出し名(Key)に対象文字列を含んでいる場合
      If (InStr(Cells(i, colKey), strContainKey) <> 0) Then
        ' 指定キーワード(文字列)が見つかった場合、対象行を削除
        If (InStr(Cells(i, colHead), strKeyword) <> 0) Then
          Cells(i, colKey).EntireRow.Delete
        End If
      End If
    Next i
  ' カンマ区切りの場合
  Else
    For j = 0 To UBound(vKey)
      strCurKey = vKey(j)
      ' 見出し名(Key)列の最終行を取得
      LastRow = Cells(Rows.Count, colKey).End(xlUp).Row

      For i = LastRow To START_ROW Step -1
        ' 見出し名(Key)に対象文字列を含んでいる場合
        If (InStr(Cells(i, colKey), strContainKey) <> 0) Then
          ' 指定キーワード(文字列)が見つかった場合、対象行を削除
          If (InStr(Cells(i, colHead), strCurKey) <> 0) Then
            Cells(i, colKey).EntireRow.Delete
          End If
        End If
      Next i
    Next j
  End If

  DeleteIssuesByKeyword = True

End Function

' １つのJIRA問処シートを１シートにコピーするFunction
' [引数]
'   strSheetMerge As String：マージ先シート名
'   strSheetA As String：マージ対象シート名A
'
' [戻り値]
'   なし
Public Function Combine1Sheets(ByVal strSheetMerge As String, _
                               ByVal strSheetA As String)

  Debug.Print "(Combine1Sheets):strSheetMerge = " & strSheetMerge & _
              " strSheetA = " & strSheetA

  ' (Pre)「Problem2_old」シートが存在する場合、削除する
  If (isSheetExist(gOldMergeSheet) = True) Then
    Application.DisplayAlerts = False
    Worksheets(gOldMergeSheet).Delete
    Application.DisplayAlerts = True
  End If

  ' (Pre)既にマージ先シートがある場合、シート名に"_old"を付けてバックアップする
  If (isSheetExist(strSheetMerge) = True) Then
    Worksheets(strSheetMerge).Name = gOldMergeSheet
  End If

  ' (1)シートAをシートごとコピーして、名前を付ける
  Worksheets(strSheetA).Copy After:=Worksheets(Worksheets.Count)
  ActiveSheet.Name = strSheetMerge

End Function

' 指定シートを作成するFunction
' (既に同名シートがある場合は、削除後に再度新規作成する)
' [引数]
'   strSheetName As String：作成するシート名
'   strAfterSheet As String：ここで指定したシート名の後ろに挿入する
'
' [戻り値]
'   なし
Public Function CreateSheet(ByVal strSheetName As String, _
                            ByVal strAfterSheet As String)
  Debug.Print "(CreateSheet):strSheetName = " & strSheetName & _
              " strAfterSheet = " & strAfterSheet

  ' 既に同名シートがある場合、削除する
  If isSheetExist(strSheetName) = True Then
    Application.DisplayAlerts = False
    Worksheets(strSheetName).Delete
    Application.DisplayAlerts = True
  End If

  Worksheets.Add After:=Worksheets(strAfterSheet)
  ActiveSheet.Name = strSheetName

  ' **** シートの書式設定 ****
  ' Font種別 = Meiryo UI
  Cells.Font.Name = "Meiryo UI"

End Function

' 指定JIRA問処シートに特定フィールド情報が存在するかをチェックするFunction
' [引数]
'   strTargetSheet As String：チェック対象(JIRA問処生データ)のシート名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function isFieldsExist(ByVal strTargetSheet As String) As Boolean
  Dim i As Long
  Dim wsTS As Worksheet
  Dim FoundCell As Range

  Debug.Print "(isFieldsExist):strTargetSheet = " & strTargetSheet

  Set wsTS = Worksheets(strTargetSheet)
  wsTS.Activate

  For i = 1 To gFieldInfo.FieldNum
    Set FoundCell = Rows(1).Find(What:=gFieldInfo.Field(i).Name, LookAt:=xlWhole)
    If FoundCell Is Nothing Then
      Debug.Print "(isFieldsExist):Field is not Exist!" & vbCrLf & _
                  "                gFieldInfo.Field(" & i & ").Name = " & _
                  gFieldInfo.Field(i).Name
      isFieldsExist = False
      Exit Function
    End If
  Next i

  isFieldsExist = True

End Function

' 指定JIRA問処シートから特定フィールド情報を抜き出して別シートに
' 出力するFunction
' [引数]
'   strMergeSheet As String：基となるJIRA問処生データのシート名
'   strFormattedSheet As String：特定フィールド抜き出し後の出力先シート名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function OutputSpecificField(ByVal strMergeSheet As String, _
                                    ByVal strFormattedSheet As String) As Boolean
  Dim wsFd As Worksheet
  Dim rowField As Long
  Dim colField As Long
  Dim rowDisp As Long
  Dim colDisp As Long
  Dim rowComp As Long
  Dim colComp As Long
  Dim i As Long
  Dim StartRow As Long
  Dim LastRow As Long
  Dim strOutput As String
  Dim strOutputField As String
  Dim FieldNum As Long

  Debug.Print "(OutputSpecificField):strMergeSheet = " & strMergeSheet & _
              " strFormattedSheet = " & strFormattedSheet

  Set wsFd = Worksheets(gFieldDefSheet)

  wsFd.Activate

  ' Field name見出し行/列を取得
  If (GetHeadPos(gFieldDefSheet, gFieldName, _
                 rowField, colField) = False) Then
    OutputSpecificField = False
    Exit Function
  End If

  ' Display見出し行/列を取得
  If (GetHeadPos(gFieldDefSheet, gDisplay, _
                 rowDisp, colDisp) = False) Then
    OutputSpecificField = False
    Exit Function
  End If

  ' Comparison見出し行/列を取得
  If (GetHeadPos(gFieldDefSheet, gComparision, _
                 rowComp, colComp) = False) Then
    OutputSpecificField = False
    Exit Function
  End If

  StartRow = rowField + 1
  LastRow = wsFd.Cells(Rows.Count, colField).End(xlUp).Row
  strOutputField = ""
  ' 「Field_def」シートの管理情報設定
  gFieldInfo.rowDataEnd = LastRow
  gFieldInfo.colDataEnd = colField
  FieldNum = 0
  ReDim gFieldInfo.Field(FieldNum)

  For i = StartRow To LastRow
    If (i = StartRow) Then
      strOutputField = "[" & wsFd.Cells(i, colField).Value & "]"
    Else
      strOutputField = strOutputField & ", [" & Cells(i, colField).Value & "]"
    End If

    FieldNum = FieldNum + 1
    ReDim Preserve gFieldInfo.Field(FieldNum)
    gFieldInfo.Field(FieldNum).Name = wsFd.Cells(i, colField).Value
    gFieldInfo.Field(FieldNum).Display = wsFd.Cells(i, colDisp).Value
    gFieldInfo.Field(FieldNum).Comparison = wsFd.Cells(i, colComp).Value
  Next i

  gFieldInfo.FieldNum = FieldNum

  Debug.Print "(OutputSpecificField):FieldNum = " & FieldNum & vbCrLf & _
              "strOutputField = " & strOutputField

  strOutput = "(OutputSpecificField):" & vbCrLf
  For i = 1 To gFieldInfo.FieldNum
    strOutput = strOutput & _
                gFieldInfo.Field(i).Name & "," & _
                gFieldInfo.Field(i).Display & "," & _
                gFieldInfo.Field(i).Comparison & vbCrLf
  Next i

  Debug.Print strOutput & _
              "gFieldInfo.rowDataEnd = " & gFieldInfo.rowDataEnd & vbCrLf & _
              "gFieldInfo.colDataEnd = " & gFieldInfo.colDataEnd

  ' 「Field_def」シートに記載のField nameがDB上(「problem2」シート)に存在
  ' するかをチェック(存在しないFieldがあるとSQL実行でエラーとなる)
  If (isFieldsExist(gMergeSheet) = False) Then
    OutputSpecificField = False
    Exit Function
  End If

  ' 出力先のシートを作成
  Call CreateSheet(strFormattedSheet, gMergeSheet)

  ' DB(Sheet)接続
  DBConnect

  ' データ基シートより、指定フィールドを抜き出して出力用シートに展開する
  Call ExpandSpecificFieldBySQL(strMergeSheet, strFormattedSheet, _
                                strOutputField)

  ' DB(Sheet)切断
  DBRelease

  OutputSpecificField = True

End Function

' 指定JIRA問処シートから全「Assignee」フィールドを読込み、
' 「Assignee_def」シートのリストへ追加するFunction
' [引数]
'   strSheetName As String：基となるJIRA問処データのシート名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function UpdateAssigneeDef(ByVal strSheetName As String) As Boolean
  Dim cList As New Collection
  Dim i As Long
  Dim j As Long
  Dim IdxAdd As Long
  Dim wsOrg As Worksheet
  Dim wsAD As Worksheet
  Dim strHead As String
  Dim rowOrg As Long
  Dim colOrg As Long
  Dim rowOrgEnd As Long
  Dim rowAD As Long
  Dim colAD As Long
  Dim rowADEnd As Long

  Debug.Print "(UpdateAssigneeDef):strSheetName = " & strSheetName

  Set wsOrg = Worksheets(strSheetName)
  Set wsAD = Worksheets(gAssigneeDefSheet)

  ' 指定シートに切替
  wsOrg.Activate

  ' 指定シートの見出し名「Assignee」の行/列を取得
  strHead = "Assignee"
  If GetHeadPos(strSheetName, strHead, _
                rowOrg, colOrg) = False Then
    UpdateAssigneeDef = False
    Exit Function
  End If

  ' 指定シートの"Assignee"列の最終データ行を取得
  rowOrgEnd = wsOrg.Cells(Rows.Count, colOrg).End(xlUp).Row

  Debug.Print "(UpdateAssigneeDef):rowOrg = " & rowOrg & _
              " colOrg = " & colOrg & " rowOrgEnd = " & rowOrgEnd

  ' "Assignee"列の全行を見て、重複しないリストを作成する。
  On Error Resume Next
  For i = rowOrg + 1 To rowOrgEnd
    cList.Add wsOrg.Cells(i, colOrg), wsOrg.Cells(i, colOrg)
  Next i
  On Error GoTo 0

  Debug.Print "(UpdateAssigneeDef):cList.Count = " & cList.Count

  ' 「Assignee_def」シートに切替
  wsAD.Activate

  ' 「Assignee_def」シートの見出し名「Assignee」の行/列を取得
  strHead = "Assignee"
  If GetHeadPos(gAssigneeDefSheet, strHead, _
                rowAD, colAD) = False Then
    UpdateAssigneeDef = False
    Exit Function
  End If

  ' 「Assignee_def」シートの"Assignee"列の最終データ行を取得
  rowADEnd = wsAD.Cells(Rows.Count, colAD).End(xlUp).Row

  ' Assigneeリストにあって、「Assignee_def」シートに記載の無いものを追記する。
  IdxAdd = 1
  For i = 1 To cList.Count
    For j = rowAD + 1 To rowADEnd
      If (wsAD.Cells(j, colAD).Value = cList(i)) Then
        GoTo SKIP_ASSIGNEE_LIST
      End If

      ' 「Assignee_def」シートに記載が無かった場合
      If (j = rowADEnd) Then
        wsAD.Cells(rowADEnd + IdxAdd, colAD) = cList(i)
        ' 罫線を引く
        wsAD.Range(wsAD.Cells(rowADEnd + IdxAdd, colAD).Address(False, False) & _
                   ":" & _
                   wsAD.Cells(rowADEnd + IdxAdd, colAD + 2).Address(False, False)). _
                   Borders.LineStyle = xlContinuous
        Debug.Print "(UpdateAssigneeDef):Add " & cList(i) & " IdxAdd = " & IdxAdd
        IdxAdd = IdxAdd + 1
      End If
    Next j
SKIP_ASSIGNEE_LIST:
  Next i

  UpdateAssigneeDef = True

End Function

' 指定シートから、２つの指定見出し名のリスト(カンマ区切り)を返すFunction
' [引数]
'   strSheetname As String：対象シート名
'   strHeadingA As String：見出し名(A)
'   strHeadingB As String：見出し名(B)
'   strListA As String：見出し名(A)に対応するリスト(カンマ区切り)
'   strListB As String：見出し名(B)に対応するリスト(カンマ区切り)
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Private Function GetCommaListFromSheet(ByVal strSheetName As String, _
                                       ByVal strHeadingA As String, _
                                       ByVal strHeadingB As String, _
                                       ByRef strListA As String, _
                                       ByRef strListB As String) As Boolean
  Dim rowHeadingA As Long
  Dim colHeadingA As Long
  Dim rowHeadingB As Long
  Dim colHeadingB As Long
  Dim StartRow As Long
  Dim EndRow As Long
  Dim i As Long

  Debug.Print "(GetCommaListFromSheet):strSheetname = " & strSheetName & _
              " strHeadingA = " & strHeadingA & " strHeadingB = " & strHeadingB & _
              " strListA = " & strListA & " strListB = " & strListB

  ' 指定シートに切替
  Worksheets(strSheetName).Activate

  ' 見出し名(A)の行/列を取得
  If GetHeadPos(strSheetName, strHeadingA, _
                rowHeadingA, colHeadingA) = False Then
    GetCommaListFromSheet = False
    Exit Function
  End If

  ' 見出し名(B)の行/列を取得
  If GetHeadPos(strSheetName, strHeadingB, _
                rowHeadingB, colHeadingB) = False Then
    GetCommaListFromSheet = False
    Exit Function
  End If

  ' 見出し名(A), (B)の全リスト値を読込んで、カンマ区切りの文字列とする
  StartRow = rowHeadingA + 1
  EndRow = Cells(Rows.Count, colHeadingA).End(xlUp).Row
  strListA = ""
  strListB = ""

  ' 指定シートより、見出し名(A), (B)の全値を読込む
  For i = StartRow To EndRow
    If (i = StartRow) Then
      strListA = Cells(i, colHeadingA).Value
      strListB = Cells(i, colHeadingB).Value
    Else
      strListA = strListA & "," & Cells(i, colHeadingA).Value
      strListB = strListB & "," & Cells(i, colHeadingB).Value
    End If
  Next i

  Debug.Print "(GetCommaListFromSheet):strListA = " & strListA & vbCrLf & _
              "                        strListB = " & strListB

  GetCommaListFromSheet = True

End Function

' ２シート間のデータを比較し、結果を指定シートに展開するFunction
' [引数]
'   strNewSheet As String：比較対象シート名(新)
'   strOldSheet As String：比較対象シート名(旧)
'   strFieldName As String：データフィールド[見出し]名群(カンマ区切りで指定)
'   strFieldComp As String：データフィールドの比較対象設定(カンマ区切りで指定)
'   strOutputSheet As String：比較結果出力先シート名
'
' [戻り値]
'   なし
Public Function Compare2SheetData(ByVal strNewSheet As String, _
                                  ByVal strOldSheet As String, _
                                  ByVal strFieldName As String, _
                                  ByVal strFieldComp As String, _
                                  ByVal strOutputSheet As String)
  Const START_ROW As Long = 2
  Const START_COL As Long = 1
  Dim wsNew As Worksheet
  Dim wsOld As Worksheet
  Dim wsOut As Worksheet
  Dim i As Long
  Dim j As Long
  Dim k As Long
  Dim vFieldName As Variant
  Dim vFieldComp As Variant
  Dim rowNewStart As Long
  Dim rowNewEnd As Long
  Dim colNewEnd As Long
  Dim rowOldStart As Long
  Dim rowOldEnd As Long
  Dim colOldEnd As Long
  Dim colOutEnd As Long
  Dim colNewField As Long
  Dim colOldField As Long
  Dim rowOutCur As Long
  Dim rngSort As Range

  Debug.Print "(Compare2SheetData):strNewSheet = " & strNewSheet & _
              " strOldSheet = " & strOldSheet & vbCrLf & _
              "                         strFieldName = " & strFieldName & vbCrLf & _
              "                         strFieldComp = " & strFieldComp & vbCrLf & _
              "                         strOutputSheet = " & strOutputSheet

  Set wsNew = Worksheets(strNewSheet)
  Set wsOld = Worksheets(strOldSheet)
  Set wsOut = Worksheets(strOutputSheet)

  ' 比較対象シート(新)の比較開始/終了行および列を取得
  wsNew.Activate
  With wsNew.UsedRange
    rowNewStart = START_ROW
    rowNewEnd = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
    colNewEnd = .Find("*", , xlFormulas, , xlByColumns, xlPrevious).Column
  End With

  Debug.Print "(Compare2SheetData):rowNewStart = " & rowNewStart & _
              " rowNewEnd = " & rowNewEnd & " colNewEnd = " & colNewEnd

  ' 比較対象シート(旧)の比較開始/終了行および列を取得
  wsOld.Activate
  With wsOld.UsedRange
    rowOldStart = START_ROW
    rowOldEnd = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
    colOldEnd = .Find("*", , xlFormulas, , xlByColumns, xlPrevious).Column
  End With

  Debug.Print "(Compare2SheetData):rowOldStart = " & rowOldStart & _
              " rowOldEnd = " & rowOldEnd & " colOldEnd = " & colOldEnd

  vFieldName = Split(strFieldName, ",")
  vFieldComp = Split(strFieldComp, ",")

  ' 比較結果出力先シートに切替
  wsOut.Activate

  ' 比較結果出力先シートの１行目はDiff結果欄とする
  wsOut.Cells(1, 1) = "Diff"
  wsOut.Cells(1, 1).Font.Bold = True

  ' 比較対象シート(新)から見出し行のみコピー
  With wsNew
    .Range(.Cells(1, 1), .Cells(1, colNewEnd)).Copy _
      Destination:=wsOut.Range("B1")
  End With

  rowOutCur = START_ROW

  ' (1)比較対象シート(新)をマスターとして、比較対象シート(旧)と、Keyを比較して
  '    変更/追加された問処を検索する
  For i = rowNewStart To rowNewEnd
    For j = rowOldStart To rowOldEnd
      ' 比較対象シート(新)/(旧)においてKeyが同じ場合、
      ' 比較対象フィールドの内容を比較
      If (wsNew.Cells(i, START_COL) = wsOld.Cells(j, START_COL)) Then
        ' 対象行を比較結果出力先シートにコピー
        With wsNew
          .Range(.Cells(i, 1), .Cells(i, colNewEnd)).Copy _
            Destination:=wsOut.Cells(rowOutCur, START_COL + 1)
        End With

        ' 比較対象として指定されているフィールドのみ比較する
        For k = LBound(vFieldComp) To UBound(vFieldComp)
          If (vFieldComp(k) = "Target") Then
            ' 指定フィールドの値が異なる場合、変更有(Mod)
            If (wsNew.Cells(i, k + 1) <> wsOld.Cells(j, k + 1)) Then
              wsOut.Cells(rowOutCur, START_COL) = "Mod"
              Debug.Print "(Compare2SheetData):Mod [" & _
                          wsNew.Cells(i, START_COL) & "]" & _
                          " [" & vFieldName(k) & "]"
              ' 対象セルの文字色を変更[赤色]
              wsOut.Cells(rowOutCur, START_COL + k + 1).Font.Color = _
                RGB(255, 0, 0)
            End If
          End If
        Next k

        ' 比較結果出力先シートの現在行をインクリメント
        rowOutCur = rowOutCur + 1
        ' 比較対象シート(新)の次の問処検索に移る
        GoTo SKIP_NEW_JUDGE
      End If
    Next j

    ' 比較対象シート(新)にしか無いKeyの場合、追加(Add)
    wsOut.Cells(rowOutCur, START_COL).Value = "Add"
    Debug.Print "(Compare2SheetData):Add [" & _
                wsNew.Cells(i, START_COL) & "]"

    ' 対象行を比較結果出力先シートにコピー
    With wsNew
      .Range(.Cells(i, 1), .Cells(i, colNewEnd)).Copy _
        Destination:=wsOut.Cells(rowOutCur, START_COL + 1)
    End With

    ' 比較結果出力先シートの現在行をインクリメント
    rowOutCur = rowOutCur + 1

SKIP_NEW_JUDGE:
  Next i

  ' (2)比較対象シート(旧)をマスターとして、比較対象シート(新)と、Keyを比較して
  '    削除された問処を検索する
  For i = rowOldStart To rowOldEnd
    For j = rowNewStart To rowNewEnd
      ' 比較対象シート(新)/(旧)においてKeyが同じ場合、削除された問処では無い
      If (wsOld.Cells(i, START_COL) = wsNew.Cells(j, START_COL)) Then
        ' 比較対象シート(旧)の次の問処検索に移る
        GoTo SKIP_OLD_JUDGE
      End If
    Next j

    ' 比較対象シート(旧)にしか無いKeyの場合、削除(Del)
    wsOut.Cells(rowOutCur, START_COL).Value = "Del"
    Debug.Print "(Compare2SheetData):Del [" & _
                wsOld.Cells(i, START_COL) & "]"

    ' 対象行を比較結果出力先シートにコピー
    With wsOld
      .Range(.Cells(i, 1), .Cells(i, colOldEnd)).Copy _
        Destination:=wsOut.Cells(rowOutCur, START_COL + 1)
    End With

    ' 比較結果出力先シートの現在行をインクリメント
    rowOutCur = rowOutCur + 1

SKIP_OLD_JUDGE:
  Next i

  ' 比較結果出力シートの最終列を取得
  With wsOut.UsedRange
    colOutEnd = .Find("*", , xlFormulas, , xlByColumns, xlPrevious).Column
  End With

  Debug.Print "(Compare2SheetData):colOutEnd = " & colOutEnd

  ' 1列目の最後まで罫線を引く
  wsOut.Range(Cells(1, 1), Cells(rowOutCur - 1, 1)).Borders.LineStyle = xlContinuous
  ' 列幅を15に設定
  wsOut.Range(Cells(1, 1), Cells(1, colOutEnd)).ColumnWidth = 15

  ' 問処番号[Key]をソートKeyにして並べ替え
  With wsOut
    .Range("A1").CurrentRegion.Sort key1:=.Range("B1"), Header:=xlYes
  End With

End Function

' 特定フィールド情報を抜き出した新/旧シートを比較して、結果を出力するFunction
' [引数]
'   strSheetNew As String：特定フィールド抜き出し後のシート名(新)
'   strSheetOld As String：特定フィールド抜き出し後のシート名(旧)
'   strOutputSheet As String：比較結果出力先シート名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function OutputComparisonResults(ByVal strSheetNew As String, _
                                        ByVal strSheetOld As String, _
                                        ByVal strOutputSheet As String) As Boolean
  Dim strFieldList As String
  Dim strCompList As String

  Debug.Print "(OutputComparisonResults):strSheetNew = " & strSheetNew & _
              " strSheetOld = " & strSheetOld & _
              " strOutputSheet = " & strOutputSheet

  ' (1)特定フィールド定義シートより「Field name」、「Comparison」のリスト
  '    (カンマ区切り)を取得する
  If (GetCommaListFromSheet(gFieldDefSheet, _
                            gFieldName, gComparision, _
                            strFieldList, strCompList) = False) Then
    OutputComparisonResults = False
    Exit Function
  End If

  ' (2)比較結果出力先シートを作成する
  Call CreateSheet(strOutputSheet, gMergeSheet)

  ' (3)２シートのデータを比較し、出力用シートに展開する
  Call Compare2SheetData(strSheetNew, strSheetOld, _
                         strFieldList, strCompList, _
                         strOutputSheet)

  OutputComparisonResults = True

End Function

' 見出し定義シートから、見出し名のリスト(カンマ区切り)、見出しのTotal数、
' ウィンドウ枠の固定表示用セルを取得するFunction
' [引数]
'   なし
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Private Function GetHeadingInfo() As Boolean
  Dim StartRow As Long
  Dim EndRow As Long
  Dim i As Long
  Dim Index As Long
  Dim rowHeadingName As Long
  Dim colHeadingName As Long
  Dim rowDataInheritance As Long
  Dim colDataInheritance As Long
  Dim rowHeadingNum As Long
  Dim colHeadingNum As Long
  Dim rowFixedDispCell As Long
  Dim colFixedDispCell As Long
  Dim strOutput As String

  ' 指定シートに切替
  Worksheets(gHeadingDefSheet).Activate

  ' 「Heading name」の行/列を取得
  If GetHeadPos(gHeadingDefSheet, gHeadingName, _
                rowHeadingName, colHeadingName) = False Then
    GetHeadingInfo = False
    Exit Function
  End If

  ' 「Data inheritance」の行/列を取得
  If GetHeadPos(gHeadingDefSheet, gDataInheritance, _
                rowDataInheritance, colDataInheritance) = False Then
    GetHeadingInfo = False
    Exit Function
  End If

  ' 「Heading number」の行/列を取得
  If GetHeadPos(gHeadingDefSheet, gHeadingNum, _
                rowHeadingNum, colHeadingNum) = False Then
    GetHeadingInfo = False
    Exit Function
  End If

  ' 「Fixed display cell」の行/列を取得
  If GetHeadPos(gHeadingDefSheet, gFixedDisplayCell, _
                rowFixedDispCell, colFixedDispCell) = False Then
    GetHeadingInfo = False
    Exit Function
  End If

  ' 「Heading number」の値を取得
  gHeadInfo.HeadNum = Cells(rowHeadingNum + 1, colHeadingNum)
  ' 「Fixed display cell」の値を取得
  gHeadInfo.FixedDisplayCell = Cells(rowFixedDispCell + 1, colFixedDispCell)

  ' グローバル変数の見出し情報配列数(動的配列)を定義
  ReDim gHeadInfo.Head(gHeadInfo.HeadNum)
  ReDim gInvInfo.Head(gHeadInfo.HeadNum)
  ReDim gInvInfoOld.Head(gHeadInfo.HeadNum)

  StartRow = rowHeadingName + 1
  EndRow = rowHeadingName + gHeadInfo.HeadNum
  Index = 1

  ' 「Heading name」を「Heading number」分読込んで情報を取得
  For i = StartRow To EndRow
    gHeadInfo.Head(Index).Name = Cells(i, colHeadingName).Value
    gHeadInfo.Head(Index).Inheritance = Cells(i, colDataInheritance).Value
    Index = Index + 1
  Next i

  strOutput = "(GetHeadingInfo):" & vbCrLf

  For i = 1 To gHeadInfo.HeadNum
    strOutput = strOutput & "    " & _
                gHeadInfo.Head(i).Name & ", " & _
                gHeadInfo.Head(i).Inheritance & vbCrLf
  Next i

  strOutput = strOutput & "    HeadNum = " & gHeadInfo.HeadNum

  Debug.Print strOutput

  GetHeadingInfo = True

End Function

' 見出し定義に従い、指定シートに問処棚卸シートを作成するFunction
' [引数]
'   strInvSheet As String：問処棚卸シート名
'   strCompSheet As String：問処差分を抽出した一時処理用シート名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function CreateInventorySheet(ByVal strInvSheet As String, _
                                     ByVal strCompSheet As String) As Boolean
  Dim i As Long
  Dim wsInv As Worksheet
  Dim wsComp As Worksheet
  Dim rowInvEnd As Long
  Dim colInvEnd As Long
  Dim rowCompEnd As Long
  Dim colCompEnd As Long

  Debug.Print "(CreateInventorySheet):strInvSheet = " & strInvSheet & _
              " strCompSheet = " & strCompSheet

  ' (1)見出し名定義シートより「Heading name」、「Heading number」を取得する
  If (GetHeadingInfo() = False) Then
    CreateInventorySheet = False
    Exit Function
  End If

  ' (2)問処棚卸シートが既に存在する場合、バックアップしておく
  '    ※既にバックアップシートが存在する場合、削除する
  If (isSheetExist(gOldInventorySheet) = True) Then
    Application.DisplayAlerts = False
    Worksheets(gOldInventorySheet).Delete
    Application.DisplayAlerts = True
  End If

  ' 既に棚卸シートが存在する場合、シート名を変更
  If (isSheetExist(strInvSheet) = True) Then
    Worksheets(strInvSheet).Name = gOldInventorySheet
  End If

  ' (3)棚卸シートを新規作成
  Worksheets.Add before:=Worksheets(Worksheets.Count)
  ActiveSheet.Name = strInvSheet

  ' **** シートの書式設定 ****
  ' Font種別 = Meiryo UI
  Cells.Font.Name = "Meiryo UI"
  ' シート全体の列幅を設定
  Cells.ColumnWidth = 15

  Set wsInv = Worksheets(strInvSheet)
  Set wsComp = Worksheets(strCompSheet)

  ' 問処棚卸シートの左端から見出し名を記載する
  For i = 1 To gHeadInfo.HeadNum
    ' 問処棚卸シートの見出し情報を更新
    gInvInfo.Head(i).Name = gHeadInfo.Head(i).Name
    gInvInfo.Head(i).rowNum = 1
    gInvInfo.Head(i).colNum = i

    ' 見出し名を入力
    wsInv.Cells(1, i) = gHeadInfo.Head(i).Name

    ' 見出し名が無い場合、列幅を小さくする
    If (gHeadInfo.Head(i).Name = "") Then
      wsInv.Columns(i).ColumnWidth = 3
    End If
  Next i

  ' 見出し全体のFontを太字に変更
  wsInv.Range(Cells(1, 1), Cells(1, gHeadInfo.HeadNum)).Font.Bold = True
  ' 見出し全体の背景色を変更
  wsInv.Range(Cells(1, 1), Cells(1, gHeadInfo.HeadNum)).Interior.Color = RGB(204, 255, 255)
  ' 見出し全体に罫線を引く
  wsInv.Range(Cells(1, 1), Cells(1, gHeadInfo.HeadNum)).Borders.LineStyle = xlContinuous

  ' (4)問処差分を抽出した一時処理用シートから、問処一覧の情報を問処棚卸シートにコピー
  ' 一時処理用シートの最終行および列を取得
  wsComp.Activate
  With wsComp.UsedRange
    rowCompEnd = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
    colCompEnd = .Find("*", , xlFormulas, , xlByColumns, xlPrevious).Column
  End With

  Debug.Print "(CreateInventorySheet):rowCompEnd = " & rowCompEnd & _
              " colCompEnd = " & colCompEnd

  ' 問処差分抽出結果を除いた問処一覧情報をコピー
  wsComp.Range(Cells(1, 2), Cells(rowCompEnd, colCompEnd)).Copy _
    Destination:=wsInv.Columns(gHeadInfo.HeadNum + 1)

  CreateInventorySheet = True

End Function

' 指定シートから、Key(問処番号)と見出しに対応する値をコピーするFunction
' [引数]
'   strSheetName As String：シート名
'   strHeadName As String：見出し名
'   strInvKey As String：Key(問処番号)名
' [戻り値]
'   Boolean：True (対応する値のコピー成功)
'            False(対応する値のコピー失敗)
Public Function CopyHeadValue(ByVal strSheetName As String, _
                              ByVal strHeadName As String, _
                              ByVal strInvKey As String) As Boolean
  Dim wsSpec As Worksheet
  Dim strSearchKey As String
  Dim i As Long
  Dim rowKey As Long
  Dim colKey As Long
  Dim rowHead As Long
  Dim colHead As Long
  Dim rowDataEnd As Long
  Dim colDataEnd As Long
  Dim rowItem As Long


  Set wsSpec = Worksheets(strSheetName)

  ' データ最終行/列を取得
  With wsSpec.UsedRange
    rowDataEnd = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
    colDataEnd = .Find("*", , xlFormulas, , xlByColumns, xlPrevious).Column
  End With

  ' 見出し(Key)の位置を特定
  strSearchKey = "Key"
  If (GetHeadPos(strSheetName, strSearchKey, _
                 rowKey, colKey) = False) Then
    Debug.Print "(CopyHeadValue):" & strSearchKey & " is not found."
    CopyHeadValue = False
    Exit Function
  End If

  ' 指定見出し位置を特定
  strSearchKey = strHeadName
  If (GetHeadPos(strSheetName, strSearchKey, _
                 rowHead, colHead) = False) Then
    Debug.Print "(CopyHeadValue):" & strSearchKey & " is not found."
    CopyHeadValue = False
    Exit Function
  End If

  ' 指定Key(問処番号)の行を特定
  rowItem = 0

  With wsSpec
    For i = 2 To rowDataEnd
      If (.Cells(i, colKey).Value = strInvKey) Then
        rowItem = i
        .Cells(i, colHead).Copy
        Exit For
      End If
    Next i
  End With

  ' 指定Key(問処番号)が見つからなかった場合、""を返す
  If (rowItem = 0) Then
    CopyHeadValue = False
    Exit Function
  End If

  CopyHeadValue = True

End Function

' 指定問処棚卸シートの各見出しに対応する値を更新するFunction
' [引数]
'   strInvSheet As String：問処棚卸シート名
'   strCompSheet As String：問処差分を抽出した一時処理用シート名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function UpdateInventoryInfo(ByVal strInvSheet As String, _
                                    ByVal strCompSheet As String) As Boolean
  Dim i As Long
  Dim j As Long
  Dim wsInv As Worksheet
  Dim wsInvOld As Worksheet
  Dim wsAssignee As Worksheet
  Dim wsComp As Worksheet
  Dim strSearchKey As String
  Dim strOutput As String
  Dim rowAssignee As Long
  Dim colAssignee As Long
  Dim rowKey As Long
  Dim colKey As Long
  Dim strTeam As String
  Dim strGroup As String
  Dim strMemo As String
  Dim strDiff As String
  Dim strVLKUPList As String
  Dim strInvKey As String

  Debug.Print "(UpdateInventoryInfo):strInvSheet = " & strInvSheet & _
              " strCompSheet = " & strCompSheet
  
  Set wsInv = Worksheets(strInvSheet)
  Set wsInvOld = Worksheets(gOldInventorySheet)
  Set wsAssignee = Worksheets(gAssigneeDefSheet)
  Set wsComp = Worksheets(strCompSheet)

  ' 問処棚卸シートに切替
  wsInv.Activate
  ' データ最終行/列を取得
  With wsInv.UsedRange
    gInvInfo.rowDataEnd = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
    gInvInfo.colDataEnd = .Find("*", , xlFormulas, , xlByColumns, xlPrevious).Column
  End With

  Debug.Print "(UpdateInventoryInfo):gInvInfo.rowDataEnd = " & gInvInfo.rowDataEnd & _
              " gInvInfo.colDataEnd = " & gInvInfo.colDataEnd

  ' 問処棚卸シート(旧)に切替
  wsInvOld.Activate
  With wsInvOld.UsedRange
    gInvInfoOld.rowDataEnd = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
    gInvInfoOld.colDataEnd = .Find("*", , xlFormulas, , xlByColumns, xlPrevious).Column
  End With

  Debug.Print "(UpdateInventoryInfo):gInvInfoOld.rowDataEnd = " & gInvInfoOld.rowDataEnd & _
              " gInvInfoOld.colDataEnd = " & gInvInfoOld.colDataEnd

  ' 問処棚卸シート(旧)において、各見出しの行/列を取得する
  For i = 1 To gHeadInfo.HeadNum
    ' 問処棚卸シート(旧)の見出し名を保存
    gInvInfoOld.Head(i).Name = gHeadInfo.Head(i).Name

    strSearchKey = gHeadInfo.Head(i).Name
    ' 見出し名が無い場合、行/列共に0とする
    If (strSearchKey = "") Then
      gInvInfoOld.Head(i).rowNum = 0
      gInvInfoOld.Head(i).colNum = 0
      GoTo SKIP_HEAD
    End If

    If (GetHeadPos(gOldInventorySheet, strSearchKey, _
                   gInvInfoOld.Head(i).rowNum, _
                   gInvInfoOld.Head(i).colNum) = False) Then
      UpdateInventoryInfo = False
      Exit Function
    End If
SKIP_HEAD:
  Next i

  strOutput = "(UpdateInventoryInfo):gInvInfoOld" & vbCrLf

  For i = 1 To gHeadInfo.HeadNum
    strOutput = strOutput & "    " & _
                gInvInfoOld.Head(i).Name & vbCrLf
  Next i

  Debug.Print strOutput

  ' 問処棚卸シートにおいて、"Key"の行/列を取得する
  strSearchKey = "Key"
  If (GetHeadPos(strInvSheet, strSearchKey, _
                 rowKey, colKey) = False) Then
    UpdateInventoryInfo = False
    Exit Function
  End If

  Debug.Print "(UpdateInventoryInfo):rowKey = " & rowKey & _
              " colKey = " & colKey

  ' 問処棚卸シートにおいて、"Assignee"の行/列を取得する
  strSearchKey = "Assignee"
  If (GetHeadPos(strInvSheet, strSearchKey, _
                 rowAssignee, colAssignee) = False) Then
    UpdateInventoryInfo = False
    Exit Function
  End If

  Debug.Print "(UpdateInventoryInfo):rowAssignee = " & rowAssignee & _
              " colAssignee = " & colAssignee

  ' 「Assignee_def」シートにおいて、各見出しの行/列を取得する
  For i = 1 To 3
    Select Case i
      Case 1
        strSearchKey = "Assignee"
      Case 2
        strSearchKey = "Team"
      Case 3
        strSearchKey = "Group"
    End Select

    gAssigneeInfo.Head(i).Name = strSearchKey

    If (GetHeadPos(gAssigneeDefSheet, strSearchKey, _
                   gAssigneeInfo.Head(i).rowNum, _
                   gAssigneeInfo.Head(i).colNum) = False) Then
      UpdateInventoryInfo = False
      Exit Function
    End If
  Next i

  ' 「Assignee_def」シートにおいて、"Assignee"リストの最終行/列を取得
  wsAssignee.Activate
  gAssigneeInfo.rowDataEnd = Cells(Rows.Count, _
                                   gAssigneeInfo.Head(1).colNum).End(xlUp).Row
  gAssigneeInfo.colDataEnd = gAssigneeInfo.Head(3).colNum

  strOutput = "(UpdateInventoryInfo):" & vbCrLf

  For i = 1 To 3
    strOutput = strOutput & "    " & _
                gAssigneeInfo.Head(i).Name & ", " & _
                gAssigneeInfo.Head(i).rowNum & ", " & _
                gAssigneeInfo.Head(i).colNum & vbCrLf
  Next i

  Debug.Print strOutput

  Debug.Print "(UpdateInventoryInfo):gAssigneeInfo.rowDataEnd = " & _
              gAssigneeInfo.rowDataEnd & _
              " gAssigneeInfo.colDataEnd = " & _
              gAssigneeInfo.colDataEnd

  ' VLOOKUPで参照するリストの範囲を作成
  strVLKUPList = wsAssignee.Cells(gAssigneeInfo.Head(1).rowNum + 1, _
                                  gAssigneeInfo.Head(1).colNum).Address & _
                 ":" & _
                 wsAssignee.Cells(gAssigneeInfo.rowDataEnd, _
                                  gAssigneeInfo.colDataEnd).Address

  Debug.Print "(UpdateInventoryInfo):strVLKUPList = " & strVLKUPList

  ' 問処棚卸シートにおいて、各見出し名毎に値を更新する
  wsInv.Activate

  For i = 2 To gInvInfo.rowDataEnd
    ' 該当行のKey(問処番号)を取得
    strInvKey = Cells(i, colKey).Value

    For j = 1 To gHeadInfo.HeadNum
      Select Case (gInvInfo.Head(j).Name)
        Case "Team"
          ' 書式を[標準]に設定
          wsInv.Range(Cells(i, j).Address).NumberFormatLocal = "G/標準"
          wsInv.Cells(i, j).Value = "=VLOOKUP(" & _
                                    Cells(i, colAssignee).Address(False, False) & "," & _
                                    gAssigneeDefSheet & "!" & strVLKUPList & "," & _
                                    "2,FALSE)"

        Case "Group"
          ' 書式を[標準]に設定
          wsInv.Range(Cells(i, j).Address).NumberFormatLocal = "G/標準"
          wsInv.Cells(i, j).Value = "=VLOOKUP(" & _
                                    Cells(i, colAssignee).Address(False, False) & "," & _
                                    gAssigneeDefSheet & "!" & strVLKUPList & "," & _
                                    "3,FALSE)"

        Case "Diff"
          ' 問処差分を抽出した一時処理用シートのDiff欄(1列目)の値をそのままコピー
          wsInv.Cells(i, j).Value = wsComp.Cells(i, 1).Value

        Case Else
          If (gHeadInfo.Head(j).Inheritance = "ON") Then
            ' 問処棚卸シート(旧)から、対応する値をコピーする
            If (CopyHeadValue(gOldInventorySheet, _
                              gHeadInfo.Head(j).Name, _
                              strInvKey) = True) Then
              ' 書式、入力規則など全てを貼り付け
              wsInv.Cells(i, j).PasteSpecial xlPasteAll
              Application.CutCopyMode = False
            End If
          End If
          
      End Select
    Next j
  Next i

  UpdateInventoryInfo = True

End Function

' 指定問処棚卸シートの各見出しに対応する値を設定するFunction
' [引数]
'   strInvSheet As String：問処棚卸シート名
'   strCompSheet As String：問処差分を抽出した一時処理用シート名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function MakeInventoryInfo(ByVal strInvSheet As String, _
                                  ByVal strCompSheet As String) As Boolean
  Dim i As Long
  Dim j As Long
  Dim wsInv As Worksheet
  Dim wsAssignee As Worksheet
  Dim wsComp As Worksheet
  Dim strSearchKey As String
  Dim strOutput As String
  Dim rowAssignee As Long
  Dim colAssignee As Long
  Dim rowKey As Long
  Dim colKey As Long
  Dim strTeam As String
  Dim strGroup As String
  Dim strMemo As String
  Dim strDiff As String
  Dim strVLKUPList As String
  Dim strInvOldSheet As String
  Dim strInvKey As String

  Debug.Print "(MakeInventoryInfo):strInvSheet = " & strInvSheet & _
              " strCompSheet = " & strCompSheet

  Set wsInv = Worksheets(strInvSheet)
  Set wsAssignee = Worksheets(gAssigneeDefSheet)
  Set wsComp = Worksheets(strCompSheet)

  ' 問処棚卸シートに切替
  wsInv.Activate
  ' データ最終行/列を取得
  With wsInv.UsedRange
    gInvInfo.rowDataEnd = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
    gInvInfo.colDataEnd = .Find("*", , xlFormulas, , xlByColumns, xlPrevious).Column
  End With

  Debug.Print "(MakeInventoryInfo):gInvInfo.rowDataEnd = " & gInvInfo.rowDataEnd & _
              " gInvInfo.colDataEnd = " & gInvInfo.colDataEnd

  ' 問処棚卸シートにおいて、"Key"の行/列を取得する
  strSearchKey = "Key"
  If (GetHeadPos(strInvSheet, strSearchKey, _
                 rowKey, colKey) = False) Then
    MakeInventoryInfo = False
    Exit Function
  End If

  Debug.Print "(MakeInventoryInfo):rowKey = " & rowKey & _
              " colKey = " & colKey

  ' 問処棚卸シートにおいて、"Assignee"の行/列を取得する
  strSearchKey = "Assignee"
  If (GetHeadPos(strInvSheet, strSearchKey, _
                 rowAssignee, colAssignee) = False) Then
    MakeInventoryInfo = False
    Exit Function
  End If

  Debug.Print "(MakeInventoryInfo):rowAssignee = " & rowAssignee & _
              " colAssignee = " & colAssignee

  ' 「Assignee_def」シートにおいて、各見出しの行/列を取得する
  For i = 1 To 3
    Select Case i
      Case 1
        strSearchKey = "Assignee"
      Case 2
        strSearchKey = "Team"
      Case 3
        strSearchKey = "Group"
    End Select

    gAssigneeInfo.Head(i).Name = strSearchKey

    If (GetHeadPos(gAssigneeDefSheet, strSearchKey, _
                   gAssigneeInfo.Head(i).rowNum, _
                   gAssigneeInfo.Head(i).colNum) = False) Then
      MakeInventoryInfo = False
      Exit Function
    End If
  Next i

  ' 「Assignee_def」シートにおいて、"Assignee"リストの最終行/列を取得
  wsAssignee.Activate
  gAssigneeInfo.rowDataEnd = Cells(Rows.Count, _
                                   gAssigneeInfo.Head(1).colNum).End(xlUp).Row
  gAssigneeInfo.colDataEnd = gAssigneeInfo.Head(3).colNum

  strOutput = "(MakeInventoryInfo):" & vbCrLf

  For i = 1 To 3
    strOutput = strOutput & "    " & _
                gAssigneeInfo.Head(i).Name & ", " & _
                gAssigneeInfo.Head(i).rowNum & ", " & _
                gAssigneeInfo.Head(i).colNum & vbCrLf
  Next i

  Debug.Print strOutput

  Debug.Print "(MakeInventoryInfo):gAssigneeInfo.rowDataEnd = " & _
              gAssigneeInfo.rowDataEnd & _
              " gAssigneeInfo.colDataEnd = " & _
              gAssigneeInfo.colDataEnd

  ' VLOOKUPで参照するリストの範囲を作成
  strVLKUPList = wsAssignee.Cells(gAssigneeInfo.Head(1).rowNum + 1, _
                                  gAssigneeInfo.Head(1).colNum).Address & _
                 ":" & _
                 wsAssignee.Cells(gAssigneeInfo.rowDataEnd, _
                                  gAssigneeInfo.colDataEnd).Address

  Debug.Print "(MakeInventoryInfo):strVLKUPList = " & strVLKUPList

  ' 問処棚卸シートにおいて、各見出し名毎に値を更新する
  wsInv.Activate
  For i = 2 To gInvInfo.rowDataEnd
    ' 該当行のKey(問処番号)を取得
    strInvKey = Cells(i, colKey).Value

    For j = 1 To gHeadInfo.HeadNum
      Select Case (gInvInfo.Head(j).Name)
        Case "Team"
          ' 書式を[標準]に設定
          wsInv.Range(Cells(i, j).Address).NumberFormatLocal = "G/標準"
          wsInv.Cells(i, j).Value = "=VLOOKUP(" & _
                                    Cells(i, colAssignee).Address(False, False) & "," & _
                                    gAssigneeDefSheet & "!" & strVLKUPList & "," & _
                                    "2,FALSE)"
        Case "Group"
          ' 書式を[標準]に設定
          wsInv.Range(Cells(i, j).Address).NumberFormatLocal = "G/標準"
          wsInv.Cells(i, j).Value = "=VLOOKUP(" & _
                                    Cells(i, colAssignee).Address(False, False) & "," & _
                                    gAssigneeDefSheet & "!" & strVLKUPList & "," & _
                                    "3,FALSE)"
        Case "Diff"
          ' 問処差分を抽出した一時処理用シートのDiff欄(1列目)の値をそのままコピー
          wsInv.Cells(i, j).Value = wsComp.Cells(i, 1).Value
        Case Else
          ' 旧版データが存在しないため、引継ぎ設定にかかわらず何もしない
          ' NOP
      End Select
    Next j
  Next i

  MakeInventoryInfo = True

End Function

' 指定シートに、別シートの行のグループ設定を継承させるFunction
' [引数]
'   strTgtSheet As String：継承対象のシート名
'   strOrgSheet As String：継承基のシート名
'
' [戻り値]
'   なし
Public Function InheritRowGroup(ByVal strTgtSheet As String, _
                                ByVal strOrgSheet As String)
  Dim wsTgt As Worksheet
  Dim wsOrg As Worksheet
  Dim GrpRowFrom As Long: GrpRowFrom = 0
  Dim i As Long
  Dim strRows As String

  Debug.Print "(InheritRowGroup):strTgtSheet = " & strTgtSheet & _
              " strOrgSheet = " & strOrgSheet
  Set wsTgt = Worksheets(strTgtSheet)
  Set wsOrg = Worksheets(strOrgSheet)

  ' 指定シートに切替
  wsTgt.Activate

  For i = 2 To gInvInfoOld.rowDataEnd
    ' 継承基の行にグループ設定されていた場合、継承対象の同一行に
    ' グループ設定する。
    If (wsOrg.Rows(i).OutlineLevel > 1) Then
      'Debug.Print "(InheritRowGroup):Rows(" & i & ").OutlineLevel = " & _
      '            wsOrg.Rows(i).OutlineLevel
      If (GrpRowFrom = 0) Then
        GrpRowFrom = i
      End If
    Else
      If ((GrpRowFrom <> 0) And (GrpRowFrom <> i - 1)) Then
        strRows = GrpRowFrom & ":" & (i - 1)
        Debug.Print "(InheritRowGroup):strRows = " & strRows
        wsTgt.Rows(strRows).Rows.Group
        GrpRowFrom = 0
      End If
    End If
    ' 最終行の場合、グループ化対象範囲に入っていればグループ化を実施する。
    If ((i = gInvInfoOld.rowDataEnd) And (GrpRowFrom <> 0)) Then
      strRows = GrpRowFrom & ":" & i
      Debug.Print "(InheritRowGroup):strRows = " & strRows
      wsTgt.Rows(strRows).Rows.Group
      GrpRowFrom = 0
    End If
  Next i

  ' 現時点でグループ化されている行を非表示にする(Gropu階層は1固定)
  wsTgt.Outline.ShowLevels RowLevels:=1

End Function

' 指定シートに、別シートの行の高さを継承させるFunction
' [引数]
'   strTgtSheet As String：継承対象のシート名
'   strOrgSheet As String：継承基のシート名
'
' [戻り値]
'   なし
Public Function InheritRowHeight(ByVal strTgtSheet As String, _
                                 ByVal strOrgSheet As String)
  Dim wsTgt As Worksheet
  Dim wsOrg As Worksheet
  Dim rowTgtKey As Long
  Dim colTgtKey As Long
  Dim rowOrgKey As Long
  Dim colOrgKey As Long
  Dim strSearchKey As String: strSearchKey = "Key"
  Dim i As Long
  Dim j As Long


  Debug.Print "(InheritRowHeight):strTgtSheet = " & strTgtSheet & _
              " strOrgSheet = " & strOrgSheet
  Set wsTgt = Worksheets(strTgtSheet)
  Set wsOrg = Worksheets(strOrgSheet)

  ' 新/旧それぞれのInv_reportシートの「Key」フィールド見出し位置を特定
  Call GetHeadPos(strTgtSheet, strSearchKey, rowTgtKey, colTgtKey)
  Call GetHeadPos(strOrgSheet, strSearchKey, rowOrgKey, colOrgKey)

  Debug.Print "(InheritRowHeight):rowTgtKey = " & rowTgtKey & _
              " colTgtKey = " & colTgtKey & vbCrLf & _
              "                   rowOrgKey = " & rowOrgKey & _
              " colOrgKey = " & colOrgKey

  For i = 2 To gInvInfo.rowDataEnd
    For j = 2 To gInvInfoOld.rowDataEnd
      ' 新/旧 Inv_reportシートにおけるKeyが同じ場合、旧Inv_reportシートの
      ' 行の高さを引き継がせる
      If (wsTgt.Cells(i, colTgtKey).Value = wsOrg.Cells(j, colOrgKey).Value) Then
        wsTgt.Rows(i).RowHeight = wsOrg.Rows(j).RowHeight
        GoTo SKIP_OLD_INV_SHEET
      End If
    Next j
SKIP_OLD_INV_SHEET:
  Next i

End Function

' 指定シートに、別シートの列幅を継承させるFunction
' [引数]
'   strTgtSheet As String：継承対象のシート名
'   strOrgSheet As String：継承基のシート名
'
' [戻り値]
'   なし
Public Function InheritColWidth(ByVal strTgtSheet As String, _
                                ByVal strOrgSheet As String)
  Dim i As Long
  Dim colEnd As Long
  Dim colWidth() As Double
  Dim wsTgt As Worksheet
  Dim wsOrg As Worksheet


  Debug.Print "(InheritColWidth):strTgtSheet = " & strTgtSheet & _
              " strOrgSheet = " & strOrgSheet
  Set wsTgt = Worksheets(strTgtSheet)
  Set wsOrg = Worksheets(strOrgSheet)

  With wsOrg
    ' 継承基のシートに切替
    .Activate

    ' グループ化されている列を表示する
    On Error Resume Next
    .Outline.ShowLevels columnLevels:=2

    colEnd = .Cells(1, Columns.Count).End(xlToLeft).Column
    Debug.Print "(InheritColWidth):colEnd = " & colEnd

    ReDim colWidth(colEnd)
    ' 継承基シートの各列幅を取得
    For i = 1 To colEnd
      colWidth(i) = .Columns(i).ColumnWidth
    Next i

    ' グループ化されている列を非表示にする
    .Outline.ShowLevels columnLevels:=1
    On Error GoTo 0
  End With

  With wsTgt
    ' 継承対象のシートに切替
    .Activate

    ' 継承対象の各列幅を設定
    For i = 1 To colEnd
      .Columns(i).ColumnWidth = colWidth(i)
    Next i
  End With

End Function

' 指定シートに、別シートの見出しセルを継承させるFunction
' [引数]
'   strTgtSheet As String：継承対象のシート名
'   strOrgSheet As String：継承基のシート名
'
' [戻り値]
'   なし
Public Function InheritHeading(ByVal strTgtSheet As String, _
                               ByVal strOrgSheet As String)
  Dim i As Long
  Dim j As Long
  Dim colTgtEnd As Long
  Dim colOrgEnd As Long
  Dim wsTgt As Worksheet
  Dim wsOrg As Worksheet


  Debug.Print "(InheritHeading):strTgtSheet = " & strTgtSheet & _
              " strOrgSheet = " & strOrgSheet
  Set wsTgt = Worksheets(strTgtSheet)
  Set wsOrg = Worksheets(strOrgSheet)

  colTgtEnd = wsTgt.Cells(1, Columns.Count).End(xlToLeft).Column
  colOrgEnd = wsOrg.Cells(1, Columns.Count).End(xlToLeft).Column
  Debug.Print "(InheritHeading):colTgtEnd = " & colTgtEnd & _
              " colOrgEnd = " & colOrgEnd

  With wsOrg
    ' 継承基のシートに切替
    .Activate

    ' グループ化されている列を表示する
    On Error Resume Next
    .Outline.ShowLevels columnLevels:=2

    ' 継承基シートの見出しセルをコピーして、継承対象の見出しセルへ貼り付け
    For i = 1 To colTgtEnd
      For j = 1 To colOrgEnd
        ' 見出し名が同じ場合のみコピーする
        If (wsTgt.Cells(1, i).Value = .Cells(1, j).Value) Then
          .Cells(1, j).Copy
          wsTgt.Cells(1, i).PasteSpecial xlPasteAll
          GoTo SKIP_ORG_HEAD
        End If
      Next j
SKIP_ORG_HEAD:
    Next i

    Application.CutCopyMode = False

    ' グループ化されている列を非表示にする
    .Outline.ShowLevels columnLevels:=1
    On Error GoTo 0
  End With

  ' 継承対象のシートに切替
  wsTgt.Activate

End Function

' 「Inv_report」シートにAutoFilter設定などを行うFunction
' [引数]
'   strInvSheet As String：問処棚卸シート名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function ConditionalFormatSetting(ByVal strInvSheet As String) As Boolean
  Dim wsFd As Worksheet
  Dim wsInv As Worksheet
  Dim rowFName As Long
  Dim colFName As Long
  Dim rowValue As Long
  Dim colValue As Long
  Dim rowColor As Long
  Dim colColor As Long
  Dim i As Long
  Dim StartRow As Long
  Dim LastRow As Long
  Dim CondNum As Long
  Dim strOutput As String
  Dim rowTarget As Long
  Dim colTarget As Long
  Dim rngTarget As Range
  Dim fc As FormatCondition
  Dim strFormula As String


  Debug.Print "(ConditionalFormatSetting):strInvSheet = " & strInvSheet
  Set wsFd = Worksheets(gFormatDefSheet)
  Set wsInv = Worksheets(strInvSheet)

  ' 「Format_def」シートに切替
  With wsFd
    .Activate

    ' Field name見出し列/行を取得
    If (GetHeadPos(gFormatDefSheet, gFieldName2, _
                   rowFName, colFName) = False) Then
      ConditionalFormatSetting = False
      Exit Function
    End If

    ' Value見出し列/行を取得
    If (GetHeadPos(gFormatDefSheet, gValue, _
                   rowValue, colValue) = False) Then
      ConditionalFormatSetting = False
      Exit Function
    End If

    ' Color見出し列/行を取得
    If (GetHeadPos(gFormatDefSheet, gColor, _
                   rowColor, colColor) = False) Then
      ConditionalFormatSetting = False
      Exit Function
    End If

    StartRow = rowValue + 1
    LastRow = .Cells(Rows.Count, colValue).End(xlUp).Row

    ' 「Format_def」シートの管理情報設定
    gFormatInfo.rowDataEnd = LastRow
    gFormatInfo.colDataEnd = colColor
    CondNum = 0
    ReDim gFormatInfo.Cond(CondNum)

    For i = StartRow To LastRow
      If (i = StartRow) Then
        gFormatInfo.FieldName = .Cells(StartRow, colFName)
      End If

      CondNum = CondNum + 1
      ReDim Preserve gFormatInfo.Cond(CondNum)
      gFormatInfo.Cond(CondNum).Value = .Cells(i, colValue)
      gFormatInfo.Cond(CondNum).Color = .Cells(i, colColor).Interior.Color
    Next i

    gFormatInfo.CondNum = CondNum

    Debug.Print "(ConditionalFormatSetting):FieldName = " & gFormatInfo.FieldName & _
                " rowDataEnd = " & gFormatInfo.rowDataEnd & _
                " colDataEnd = " & gFormatInfo.colDataEnd
    strOutput = "(ConditionalFormatSetting):CondNum = " & _
                gFormatInfo.CondNum & vbCrLf
    For i = 1 To gFormatInfo.CondNum
      strOutput = strOutput & _
                  gFormatInfo.Cond(i).Value & ", " & _
                  gFormatInfo.Cond(i).Color & vbCrLf
    Next i
    Debug.Print strOutput

  End With

  ' 指定シートに切替
  With wsInv
    .Activate

    ' 一旦、条件付き書式を全削除
    Set rngTarget = .Range(.Cells(2, 1), _
                           .Cells(gInvInfo.rowDataEnd, gInvInfo.colDataEnd))
    rngTarget.FormatConditions.Delete

    ' Field nameが指定されていない場合、条件付き書式設定は行わない
    If (gFormatInfo.FieldName = "") Then
      Debug.Print "(ConditionalFormatSetting):Field name is not specified."
      ConditionalFormatSetting = True
      Exit Function
    End If

    ' 指定Field nameの見出しがInv_repotシートにあるか検索
    If (GetHeadPos(gInventorySheet, gFormatInfo.FieldName, _
                   rowTarget, colTarget) = False) Then
      MsgBox "[Format_def]指定Field nameが見つかりませんでした。", _
             vbCritical
      ConditionalFormatSetting = False
      Exit Function
    End If

    Debug.Print "(ConditionalFormatSetting):rowTarget = " & rowTarget & _
                " colTarget = " & colTarget

    For i = 1 To gFormatInfo.CondNum
      ' 条件付き書式の条件を設定
      strFormula = "=$" & .Cells(rowTarget + 1, colTarget).Address(False, False) & _
                   "=""" & gFormatInfo.Cond(i).Value & """"
      Debug.Print "(ConditionalFormatSetting):i = " & i & _
                  " strFormula = " & strFormula
      Set fc = rngTarget.FormatConditions.Add(xlExpression, _
                                              xlEqual, _
                                              strFormula)
      Set fc = rngTarget.FormatConditions(i)
      ' 条件付き書式の書式を設定
      fc.Interior.Color = gFormatInfo.Cond(i).Color
    Next i

  End With

  ConditionalFormatSetting = True

End Function

' 「Field_def」シートのDisplay設定に従って、指定問処棚卸シートの各見出しの
' グループ化/非表示設定を行うFunction
' [引数]
'   strInvSheet As String：問処棚卸シート名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function HeadGroupSetting(ByVal strInvSheet As String) As Boolean
  Dim i As Long
  Dim rowTmp As Long
  Dim colTmp As Long
  Dim wsInv As Worksheet

  Debug.Print "(HeadGroupSetting):strInvSheet = " & strInvSheet
  Set wsInv = Worksheets(strInvSheet)

  ' 指定シートに切替
  wsInv.Activate
  ' 列のグループ化を全て削除
  wsInv.Columns.ClearOutline

  For i = 1 To gFieldInfo.FieldNum
    Select Case (gFieldInfo.Field(i).Display)
      Case "Hide"
        ' 該当フィールド見出しの行/列を取得
        If (GetHeadPos(strInvSheet, _
                       gFieldInfo.Field(i).Name, _
                       rowTmp, colTmp) = False) Then
          HeadGroupSetting = False
          Exit Function
        End If

        ' グループ化設定を行う
        wsInv.Range(Cells(rowTmp, colTmp).Address).Columns.Group
      Case Else
        ' NOP
    End Select
  Next i

  ' 現時点でグループ化されている個所を非表示にする(Gropu階層は1固定)
  wsInv.Outline.ShowLevels columnLevels:=1

  HeadGroupSetting = True

End Function

' 「Inv_report」シートにAutoFilter設定などを行うFunction
' [引数]
'   strInvSheet As String：問処棚卸シート名
'
' [戻り値]
'   なし
Public Function SheetSetting(ByVal strInvSheet As String)
  Dim wsInv As Worksheet

  Debug.Print "(SheetSetting):strInvSheet = " & strInvSheet
  Set wsInv = Worksheets(strInvSheet)

  ' 指定シートに切替
  With wsInv
    .Activate

    ' AutoFilterの設定状態を確認し、既に設定済みの場合は解除する
    If (.AutoFilterMode = True) Then
      .Range("A1").AutoFilter
    End If

    ' 1列目の空白で無いセルを絞り込み
    .Range("A1").AutoFilter 1, "<>"
    ' 絞り込み解除
    .ShowAllData

    ' ウィンドウ枠の固定
    On Error GoTo ErrLabel
    .Range(gHeadInfo.FixedDisplayCell).Select
    ActiveWindow.FreezePanes = False
    ActiveWindow.FreezePanes = True
    On Error GoTo 0

    ' シート見出しの色を変更(deeppink)
    .Tab.Color = RGB(255, 20, 147)
  End With

  Exit Function

ErrLabel:
  MsgBox "「" & gHeadingDefSheet & "」シートの[" & _
         gFixedDisplayCell & "]の指定が間違っています。"
  Resume Next
End Function
