Attribute VB_Name = "Inventory_Main"
Option Explicit

' 参照設定
'
' Microsoft ActiveX Data Objects 6.1 Library
' Microsoft Scripting Runtime
' Windows Script Host Object Model
'
' ************************
' JIRA問処棚卸ツール用定義
' ************************
' DB Access関連
Public objCon As Object
Public objRS As Object

' シート名
Public Const gMenuSheet As String = "Analyze"
Public Const gMergeSheet As String = "Problem2"
Public Const gOldMergeSheet As String = "Problem2_old"
Public Const gNewSheet As String = "Formatted"
Public Const gOldSheet As String = "Formatted_old"
Public Const gFieldDefSheet As String = "Field_def"
Public Const gHeadingDefSheet As String = "Heading_def"
Public Const gAssigneeDefSheet As String = "Assignee_def"
Public Const gFormatDefSheet As String = "Format_def"
Public Const gJiraRawData As String = gJIRASheetName  ' JIRA_RawData

' Field_defシートに記載するパラメータ定義名
Public Const gFieldName As String = "Field name"
Public Const gDisplay As String = "Display"
Public Const gComparision As String = "Comparison"

' Heading_defシートに記載するパラメータ定義名
Public Const gHeadingName As String = "Heading name"
Public Const gDataInheritance As String = "Data inheritance"
Public Const gHeadingNum As String = "Heading number"
Public Const gFixedDisplayCell As String = "Fixed display cell"

' Format_defシートに記載するパラメータ定義名
Public Const gFieldName2 As String = "Field name"
Public Const gValue As String = "Value"
Public Const gColor As String = "Color"

' JIRA問処棚卸データ出力用シート名
Public Const gInventorySheet As String = "Inv_report"
Public Const gOldInventorySheet As String = "Inv_report_old"
' JIRA問処間引き用参照見出し名
Public Const gKeyHead As String = "Key"

Type tHeadInfoA
  Name As String
  rowNum As Long
  colNum As Long
End Type

' 「Assignee_def」シートの見出し関連情報
Type tAssigneeInfo
  Head(3) As tHeadInfoA    ' 見出し情報(名前/行/列)
  rowDataEnd As Long       ' データ最終行
  colDataEnd As Long       ' データ最終列
End Type

Public gAssigneeInfo As tAssigneeInfo

Type tHeadInfoB
  Name As String
  Inheritance As String
End Type

' 「Heading_def」シートの見出し関連情報
Type tHeadInfo
  Head() As tHeadInfoB       ' 見出し情報(名前/データ継承設定/行/列)
  HeadNum As Long            ' 見出しの総数
  FixedDisplayCell As String ' ウィンドウ枠の固定表示用セル
End Type

Public gHeadInfo As tHeadInfo

Type tFieldInfo
  Name As String
  Display As String
  Comparison As String
End Type

' 「Field_def」シートの見出し関連情報
Type tFDInfo
  Field() As tFieldInfo    ' Field情報(名前/非表示設定/データ比較設定)
  FieldNum As Long         ' Field name総数
  rowDataEnd As Long       ' データ最終行
  colDataEnd As Long       ' データ最終列
End Type

Public gFieldInfo As tFDInfo

Type tCondInfo
  Value As String
  Color As Long
End Type

' 「Format_def」シートの見出し関連情報
Type tFMTInfo
  FieldName As String     ' 条件とするフィールド名
  Cond() As tCondInfo     ' 条件パラメータ(値：文字列/色：RGB)
  CondNum As Long         ' 条件の総数
  rowDataEnd As Long      ' データ最終行
  colDataEnd As Long      ' データ最終列
End Type

Public gFormatInfo As tFMTInfo

' Issue_inventory_reportシートの見出し関連情報
Type tInvInfo
  Head() As tHeadInfoA    ' 見出し情報(名前/データ継承設定/行/列)
  rowDataEnd As Long      ' データ最終行
  colDataEnd As Long      ' データ最終列
End Type

Public gInvInfo As tInvInfo
Public gInvInfoOld As tInvInfo

'*******************************************************************************
' 自動実行用マクロ
'
' [引数]
' なし
'
' [戻り値]
' なし
'
' [処理概要]
' (Step 1)CurlでJIRAデータを取得(JiraGet2nd)
' (Step 2)棚卸設定に従い、棚卸データを作成
'*******************************************************************************
Public Sub PerformInventoryAllExec()

  Call GetJiraData(False)
  Call btnPerformInventory(False)

End Sub

'*******************************************************************************
' Perform inventoryボタンをクリック
'
' [引数]
' なし
'
' [戻り値]
' なし
'
' [処理概要]
' (Step 1)JIRA生データ「JIRA_RawData」シートから、指定見出し名＆指定キーワード
'         [文字列]で検索し、条件に一致するデータ[問処]を削除する。
' (Step 2)(Step 1)で間引き処理を行ったデータをシートに出力する。
'         (前データがある場合、シート名に"_old"を付けてバックアップしておく)
' (Step 3)(Step 2)で出力したデータより、指定フィールドの情報を抜き出して別シート
'         にコピーする。
' (Step 4)(Step 2)でバックアップした前版のJIRA生データから指定フィールド
'         の情報を抜き出して別シートにコピーする。
'         ※前版のJIRA生データが無い場合は、ここで終了する。
' (Step 5)(Step 3),(Step 4)で生成したデータの前版と現版を比較し、差分を抽出して
'         別シートに結果を出力する。
' (Step 6)指定された見出し定義に従い、問処棚卸シートの見出しを作成する。
' (Step 7)(Step 5)の内容を問処棚卸シートにコピーする。
' (Step 8)問処棚卸シートの前版情報を基に、各見出しに対応する値を更新する。
'         ※問処棚卸シートの前版情報が無い場合、値の更新は行わない。
'*******************************************************************************
Public Sub btnPerformInventory(Optional ByVal bEndMsg As Boolean = True)
  Dim StartTime As Double
  Dim EndTime As Double
  Dim TotalTime As Double
  Const strTempSheet As String = "JIRATempData"
  Const strTempSheetComp As String = "TempCompData"
  Dim strCurSheetName As String

  ' 開始時間取得
  StartTime = Timer

  ' プログレスバーFormを表示
  Info.Show vbModeless

  Application.ScreenUpdating = False
  Application.Calculation = xlCalculationManual

  strCurSheetName = ActiveSheet.Name
  Debug.Print "(btnPerformInventory):strCurSheetName = " & strCurSheetName

  ' JIRA PTR dataのExclude設定パラメータを取得
  Call GetModeExclude

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, "Get setting process finished.")
  Call UpdateProgressBar(10)

  ' (Step 1)JIRA生データ「JIRA_RawData」シートから、指定見出し名＆指定キーワード
  '         [文字列]で検索し、条件に一致するデータ[問処]を削除する。
  If (isSheetExist(gJiraRawData) = False) Then
    ' プログレスバーFormを閉じる
    Unload Info
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    MsgBox "Processing Failed..." & vbCrLf & _
           gJiraRawData & " sheet is not found.", vbCritical
    Exit Sub
  End If

  ' 間引き処理するシートを一時処理用シートにコピーする
  Worksheets(gJiraRawData).Copy After:=Worksheets(gJiraRawData)
  ActiveSheet.Name = strTempSheet

  If (gSettingFID.ExKey <> "") And _
     (gSettingFID.ExField <> "") And _
     (gSettingFID.ExStr <> "") Then
    ' 対象問処において、指定フィールドに指定文字列があるものを削除する。
    If (DeleteIssuesByKeyword(strTempSheet, _
                              gSettingFID.ExKey, _
                              gSettingFID.ExField, _
                              gSettingFID.ExStr) = False) Then
      ' プログレスバーFormを閉じる
      Unload Info
      Application.ScreenUpdating = True
      Application.Calculation = xlCalculationAutomatic
      MsgBox "Processing Failed..." & vbCrLf & _
             "(DeleteIssuesByKeyword Fail in " & strTempSheet & ")", _
             vbCritical
      Exit Sub
    End If
  End If

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, "Data exclude process finished.")
  Call UpdateProgressBar(20)

  ' (Step 2)(Step 1)で間引き処理を行ったデータをシートに出力する。
  '         (前データがある場合、シート名に"_old"を付けてバックアップしておく)
  ' シート(間引き済み)を１つのSheetにまとめる
  Call Combine1Sheets(gMergeSheet, strTempSheet)
  ' 一時処理用シートを削除する
  Application.DisplayAlerts = False
  Worksheets(strTempSheet).Delete
  Application.DisplayAlerts = True

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, _
                       "Make " & gMergeSheet & " sheet process finished.")
  Call UpdateProgressBar(30)

  ' (Step 3)(Step 2)で出力したデータより、指定フィールドの情報を抜き出して別シート
  '         にコピーする。

  ' 「Problem2」シートをDBとして読込み、SQLを使用して「Field_def」に記載の
  ' フィールド情報を抽出して「Formatted」シートに出力する。
  If (OutputSpecificField(gMergeSheet, gNewSheet) = False) Then
    ' プログレスバーFormを閉じる
    Unload Info
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    MsgBox "Processing Failed..." & vbCrLf & _
           "(OutputSpecificField Fail from " & gMergeSheet & ")", _
           vbCritical
    Exit Sub
  End If

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, _
                       "Make " & gNewSheet & " sheet process finished.")
  Call UpdateProgressBar(40)

  ' 「Formatted」シートの"Assignee"フィールドを読込み、「Assignee_def」
  ' シートに記載の無いものを追加する。
  If (UpdateAssigneeDef(gNewSheet) = False) Then
    ' プログレスバーFormを閉じる
    Unload Info
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    MsgBox "Processing Failed..." & vbCrLf & _
           "(UpdateAssigneeDef Fail from " & gNewSheet & ")", _
           vbCritical
    Exit Sub
  End If

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, _
                       "Update " & gAssigneeDefSheet & " sheet process finished.")
  Call UpdateProgressBar(50)

  ' (Step 4)(Step 2)でバックアップした前版のマージ済みJIRA生データから指定フィールド
  '         の情報を抜き出して別シートにコピーする。

  ' 「Problem2_old」シートが無い場合、再度JIRAデータを取得する様に促すメッセージ
  ' を表示して終了
  If (isSheetExist(gOldMergeSheet) = False) Then
    ' Bookを保存
    ActiveWorkbook.Save

    ' シートを非表示
    Worksheets(gMergeSheet).Visible = False
    Worksheets(gNewSheet).Visible = False

    ' 元のシートへ戻る
    Worksheets(strCurSheetName).Activate

    ' プログレスバーFormを閉じる
    Unload Info
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    MsgBox "No old JIRA data to compare!" & vbCrLf & _
           "Please get the JIRA data again.", vbInformation
    Exit Sub
  End If

  ' シートを再表示
  Worksheets(gOldMergeSheet).Visible = True

  ' 「Problem2_old」シートをDBとして読込み、SQLを使用して「Field_def」に記載の
  ' フィールド情報を抽出して「Formatted_old」シートに出力する。
  If (OutputSpecificField(gOldMergeSheet, gOldSheet) = False) Then
    ' プログレスバーFormを閉じる
    Unload Info
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    MsgBox "Processing Failed..." & vbCrLf & _
           "(OutputSpecificField Fail from " & gOldMergeSheet & ")", vbCritical
    Exit Sub
  End If

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, _
                       "Make " & gOldSheet & " sheet process finished.")
  Call UpdateProgressBar(60)

  ' (Step 5)(Step 3),(Step 4)で生成したデータの前版と現版を比較し、差分を抽出して
  '         別シートに結果を出力する。

  ' 「Formatted」、「Formatted_old」シートを比較し、差分を抽出して
  ' 一時処理用シートに出力する。
  If (OutputComparisonResults(gNewSheet, gOldSheet, _
                              strTempSheetComp) = False) Then
    ' プログレスバーFormを閉じる
    Unload Info
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    MsgBox "Processing Failed..." & vbCrLf & _
           "(OutputComparisonResults Fail)", vbCritical
    Exit Sub
  End If

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, _
                       "Compare " & gNewSheet & "/" & gOldSheet & _
                       " sheet process finished.")
  Call UpdateProgressBar(70)

  ' (Step 6)指定された見出し定義に従い、問処棚卸シートの見出しを作成する。
  ' (Step 7)(Step 5)の内容を問処棚卸シートにコピーする。
  If (CreateInventorySheet(gInventorySheet, strTempSheetComp) = False) Then
    ' プログレスバーFormを閉じる
    Unload Info
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    MsgBox "Processing Failed..." & vbCrLf & _
           "(CreateInventorySheet Fail)", vbCritical
  End If

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, _
                       "Make " & gInventorySheet & " sheet process finished.")
  Call UpdateProgressBar(80)

  ' 前版の棚卸データが有る場合
  If (isSheetExist(gOldInventorySheet) = True) Then
    ' (Step 8)問処棚卸シートの各見出しに対応する値を更新する。
    If (UpdateInventoryInfo(gInventorySheet, strTempSheetComp) = False) Then
      ' プログレスバーFormを閉じる
      Unload Info
      Application.ScreenUpdating = True
      Application.Calculation = xlCalculationAutomatic
      MsgBox "Processing Failed..." & vbCrLf & _
             "(UpdateInventoryInfo Fail)", vbCritical
    End If

    ' 前版シートの見出しセル(1行目)を継承する
    Call InheritHeading(gInventorySheet, gOldInventorySheet)

    ' 前版シートの列幅を継承する
    Call InheritColWidth(gInventorySheet, gOldInventorySheet)

    ' 前版シートの行の高さを継承する
    Call InheritRowHeight(gInventorySheet, gOldInventorySheet)

  ' 前版の棚卸データが無い場合
  Else
    If (MakeInventoryInfo(gInventorySheet, strTempSheetComp) = False) Then
      ' プログレスバーFormを閉じる
      Unload Info
      Application.ScreenUpdating = True
      Application.Calculation = xlCalculationAutomatic
      MsgBox "Processing Failed..." & vbCrLf & _
             "(MakeInventoryInfo Fail)", vbCritical
    End If
  End If

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, _
                       "Update " & gInventorySheet & " sheet process finished.")
  Call UpdateProgressBar(90)

  ' 問処棚卸しシートにおいて、「Format_def」シートに記載の条件付き書式設定を行う。
  If (ConditionalFormatSetting(gInventorySheet) = False) Then
    ' プログレスバーFormを閉じる
    Unload Info
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    MsgBox "Processing Failed..." & vbCrLf & _
           "(ConditionalFormatSetting Fail)", vbCritical
  End If

  ' 問処棚卸シートにおいて、グループ化の設定/非表示設定を行う。
  If (HeadGroupSetting(gInventorySheet) = False) Then
    ' プログレスバーFormを閉じる
    Unload Info
    Application.ScreenUpdating = True
    Application.Calculation = xlCalculationAutomatic
    MsgBox "Processing Failed..." & vbCrLf & _
           "(HeadGroupSetting Fail)", vbCritical
  End If

  ' 問処棚卸シートにおいて、AutoFilter設定などを行う。
  Call SheetSetting(gInventorySheet)

  ' 前版シートの行のグループ化設定を継承する
  Call InheritRowGroup(gInventorySheet, gOldInventorySheet)

  ' 差分出力用の一時処理用シートを削除
  Application.DisplayAlerts = False
  Worksheets(strTempSheetComp).Delete
  Application.DisplayAlerts = True

  ' Bookを保存
  'Application.DisplayAlerts = False
  'ActiveWorkbook.Save
  'Application.DisplayAlerts = True

  ' シートを非表示
  'Worksheets(gJiraRawData).Visible = False
  Worksheets(gMergeSheet).Visible = False
  Worksheets(gOldMergeSheet).Visible = False
  Worksheets(gNewSheet).Visible = False
  Worksheets(gOldSheet).Visible = False

  ' 元のシートへ戻る
  Worksheets(strCurSheetName).Activate

  Application.ScreenUpdating = True

  ' 経過時間をデバッグ表示
  Call DispElapsedTime(StartTime, "All process Complete：")
  Call UpdateProgressBar(100)

  ' プログレスバーFormを閉じる
  Unload Info
  Application.ScreenUpdating = True
  Application.Calculation = xlCalculationAutomatic

  ' 終了時間取得
  EndTime = Timer
  TotalTime = EndTime - StartTime

  If (bEndMsg = True) Then
    MsgBox "Processing Complete!" & vbCrLf & _
           "elapsed time : " & TotalTime, vbInformation
  End If

End Sub

' 現在開いているBookをDBとした接続を行うFunction
' [引数]
'   なし
'
' [戻り値]
'   なし
Public Sub DBConnect(Optional ByVal ReadOnly As Integer = 1)

    ' ADOコネクションのオブジェクトを作成
    Set objCon = CreateObject("ADODB.Connection")
    ' ReadOnly=1 読み込みのみ
    objCon.Open "Driver={Microsoft Excel Driver (*.xls, *.xlsx, *.xlsm, *.xlsb)};" & _
                "DBQ=" & ThisWorkbook.FullName & ";" & _
                "ReadOnly=" & ReadOnly
    objCon.CursorLocation = adUseClient

    ' ADOレコードセットのオブジェクトを作成
    Set objRS = CreateObject("ADODB.Recordset")
End Sub

' 現在開いているBookをDBとした接続を解除するFunction
' [引数]
'   なし
'
' [戻り値]
'   なし
Public Sub DBRelease()

    If Not (objRS Is Nothing) Then
        objRS.Close
        Set objRS = Nothing
    End If
       
    If Not (objCon Is Nothing) Then
        objCon.Close
        Set objCon = Nothing
    End If

End Sub

' SQLを用いて、指定フィールド情報を抜き出して別シートに展開するFunction
' [引数]
'   strDataName As String：基となるデータが存在するシート名
'   strExpandName As String：データ展開先のシート名
'   strFieldName As String：抜き出すフィールド名群(カンマ区切りで指定)
'
' [戻り値]
'   なし
Public Function ExpandSpecificFieldBySQL(ByVal strDataName As String, _
                                         ByVal strExpandName As String, _
                                         ByVal strFieldName As String)

  Dim i As Long
  Dim strSQL As String
  Dim rngData As Range

  Debug.Print "(ExpandSpecificFieldBySQL):strDataName = " & strDataName & _
              " strExpandName = " & strExpandName & _
              " strFieldName = " & strFieldName

  ' (1)SQLによるデータ処理
  strSQL = "SELECT " & _
           strFieldName & _
           " FROM [" & strDataName & "$]"
  Set objRS = objCon.Execute(strSQL)

  ' (2)出力処理
  ' 列の見出し
  For i = 0 To objRS.Fields.Count - 1
    Sheets(strExpandName).Range("A1").Offset(0, i).Value = objRS.Fields(i).Name
    ' 見出し文字を太字に設定
    Sheets(strExpandName).Range("A1").Offset(0, i).Font.Bold = True
  Next
  ' データ
  Sheets(strExpandName).Range("A2").CopyFromRecordset objRS

  ' 全体に罫線を引く
  Set rngData = Sheets(strExpandName).UsedRange
  rngData.Borders.LineStyle = xlContinuous

  ' (3)列幅を設定
  For i = 1 To objRS.Fields.Count
    Columns(i).ColumnWidth = 15
  Next

End Function

' SQLを用いて、２シート間のデータを比較し、結果を別シートに展開するFunction
' [引数]
'   strSheetA As String：比較対象シート名(新)
'   strSheetB As String：比較対象シート名(旧)
'   strFieldName As String：抜き出すフィールド名群(カンマ区切りで指定)
'   strFieldComp As String：該当フィールドの比較対象設定(カンマ区切りで指定)
'   strOutputSheet As String：比較結果出力先シート名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Public Function Compare2SheetDataBySQL(ByVal strSheetA As String, _
                                       ByVal strSheetB As String, _
                                       ByVal strFieldName As String, _
                                       ByVal strFieldComp As String, _
                                       ByVal strOutputSheet As String) As Boolean
  Const strDATA_A As String = "DATA_A"
  Const strDATA_B As String = "DATA_B"
  Const strTgt As String = "Target"
  Dim i As Long
  Dim vField As Variant
  Dim vComp As Variant
  Dim strSQLAdd As String
  Dim strSQLDel As String
  Dim strSQLDiff As String
  Dim strSQLJoin As String

  Debug.Print "(Compare2SheetDataBySQL):strSheetA = " & strSheetA & _
              " strSheetB = " & strSheetB & vbCrLf & _
              "                         strFieldName = " & strFieldName & vbCrLf & _
              "                         strFieldComp = " & strFieldComp & vbCrLf & _
              "                         strOutputSheet = " & strOutputSheet

  Worksheets(strOutputSheet).Activate

  vField = Split(strFieldName, ",")
  vComp = Split(strFieldComp, ",")

  ' (1)追加問処(旧データには無い問処)があるかのSQL判定文
  ' 旧データ側のKeyが無い場合、追加とみなして"Add"を表示
  strSQLAdd = "SELECT IIf(" & strDATA_B & "." & vField(0) & _
            " Is Null, 'Add', '') AS Diff, "
  ' 新データ側の全フィールドを取得する
  For i = LBound(vField) To UBound(vField)
    If (i = LBound(vField)) Then
      strSQLAdd = strSQLAdd & strDATA_A & "." & vField(i)
    Else
      strSQLAdd = strSQLAdd & ", " & strDATA_A & "." & vField(i)
    End If
  Next i
  ' 新旧データで、Keyが同じデータを結合して、旧データ側のKeyが無いデータに
  ' 限定して抽出する
  strSQLAdd = strSQLAdd & _
              " FROM [" & strSheetA & "$] as " & strDATA_A & _
              " LEFT JOIN [" & strSheetB & "$] as " & strDATA_B & " ON " & _
              strDATA_A & "." & vField(0) & " = " & strDATA_B & "." & vField(0) & _
              " WHERE (((" & strDATA_B & "." & vField(0) & ") Is Null))"

  ' (2)削除問処(新データには無い問処)があるかのSQL判定文
  strSQLDel = "SELECT IIf(" & strDATA_A & "." & vField(0) & _
            " Is Null, 'Del', '') AS Diff, "
  ' 旧データ側の全フィールドを取得する
  For i = LBound(vField) To UBound(vField)
    If (i = LBound(vField)) Then
      strSQLDel = strSQLDel & strDATA_B & "." & vField(i)
    Else
      strSQLDel = strSQLDel & ", " & strDATA_B & "." & vField(i)
    End If
  Next i
  ' 新旧データで、Keyが同じデータを結合して、新データ側のKeyが無いデータに
  ' 限定して抽出する
  strSQLDel = strSQLDel & _
              " FROM [" & strSheetB & "$] as " & strDATA_B & _
              " LEFT JOIN [" & strSheetA & "$] as " & strDATA_A & " ON " & _
              strDATA_B & "." & vField(0) & " = " & strDATA_A & "." & vField(0) & _
              " WHERE (((" & strDATA_A & "." & vField(0) & ") Is Null))"

  ' (3)指定フィールドに差分があるかのSQL判定文
  strSQLDiff = "SELECT "

  ' Comparisonに"Target"指定されているFieldのみ差分を比較する
  For i = LBound(vComp) To UBound(vComp)
    If (vComp(i) = strTgt) Then
      strSQLDiff = strSQLDiff & "IIF((" & strDATA_B & "." & vField(i) & " = " & _
                   strDATA_A & "." & vField(i) & "),'No change','Modified') AS Diff" & _
                   i & ", "
    End If
  Next i

  ' 新データ側の全フィールドを取得する
  For i = LBound(vField) To UBound(vField)
    If (i = LBound(vField)) Then
      strSQLDiff = strSQLDiff & strDATA_A & "." & vField(i)
    Else
      strSQLDiff = strSQLDiff & ", " & strDATA_A & "." & vField(i)
    End If
  Next i

  ' 新旧データで、Keyが同じデータを結合して、新データ側のデータを抽出する
  strSQLDiff = strSQLDiff & _
               " FROM [" & strSheetB & "$] as " & strDATA_B & _
               " INNER JOIN [" & strSheetA & "$] as " & strDATA_A & " ON " & _
               strDATA_B & "." & vField(0) & " = " & strDATA_A & "." & vField(0)

  ' (4)結合
  strSQLJoin = "select * from (" & strSQLAdd & ") " & _
                 "union select * from (" & strSQLDel & ") " & _
                 "union select * from (" & strSQLDiff & ") " & _
                 "order by " & vField(0)

  Set objRS = objCon.Execute(strSQLJoin)

  ' (5)出力処理
  ' 見出し
  For i = 0 To objRS.Fields.Count - 1
    Sheets(strOutputSheet).Range("A1").Offset(0, i).Value = objRS.Fields(i).Name
  Next i
  ' データ
  Sheets(strOutputSheet).Range("A2").CopyFromRecordset objRS

  Compare2SheetDataBySQL = True

End Function

' プログレスバーの処理進捗率を更新するFunction
' [引数]
'   percent As Long：プログレスバーに表示する％値
'
' [戻り値]
'   なし
Public Function UpdateProgressBar(ByVal Percent As Long)
  With Info
    .ProgressBar1.Value = Percent
    .Percent.Caption = Int(Percent / .ProgressBar1.Max * 100) & "%"
    .Repaint
  End With
End Function

' 途中までの経過時間をデバッグ表示するFunction
' [引数]
'   StartTime As Double ：処理開始時間
'   strDispMsg As String：経過時間の前に付与するラベル文字列
'
' [戻り値]
'   なし
Public Function DispElapsedTime(ByVal StartTime As Double, _
                                ByVal strDispMsg As String)
  Dim MiddleTime As Double
  Dim ProcessTime As Double

  MiddleTime = Timer
  ProcessTime = MiddleTime - StartTime
  Debug.Print strDispMsg; ProcessTime

End Function

