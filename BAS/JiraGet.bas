Attribute VB_Name = "JiraGet"
Option Explicit

Public Const gConfigSheetName As String = "DL"
Public Const gJIRASheetName As String = "JIRA_RawData"
Public Const gSettingSheetName As String = "Setting"
Public Const gFIDRangeName As String = "ConfigFilterID"
Public Const gBaseURLRangeName As String = "BaseURL"
Public Const gUserIDRangeName As String = "UserID"
Public Const gPasswordRangeName As String = "Password"
Public Const gCurlPathRangeName As String = "CurlPath"
Public Const gJiraImportBtn As String = "Jira_Import_Btn"
Public Const gFuncImportBtn As String = "Func_Import_Btn"
Public Const gCheckBoxFidAll As String = "CheckBox_FID_All"
Public Const gCheckBoxFidCur As String = "CheckBox_FID_Cur"

' 日付変更が必要なJIRAデータのField name定義
Public Const gDateConvField As String = _
  "Created,Last Viewed,Updated,Resolved,Due Date,Done Date,Target Fix Date,Planned Start Date,Target Analysis Date"

' JIRA PTR Listのフィールド設定定義
Public Enum eFieldsSet
  Error = 0
  all = 1
  Current = 2
End Enum

' FID等の管理情報
Type tFIDSet
  Id As String           ' フィルターID
  OutputPath As String   ' 出力パス名(ファイル名込み)
  FieldsOp As eFieldsSet ' フィールド選択状態
  ExKey As String        ' Keyに含まれる文字列
  ExField As String      ' 見出し名の文字列
  ExStr As String        ' 検索対象文字列
End Type

Type tBaseInfo
  BaseURL As String
  UserID As String
  Password As String
  CurlPath As String
End Type

Public gSettingFID As tFIDSet
Public gSettingBase As tBaseInfo


'*******************************************************************************
' 自動実行用マクロ
'
' [引数]
' なし
'
' [戻り値]
' なし
'
' [処理概要]
' (Step 1)Curlで指定フィルタIDのJIRAデータを取得する。
'*******************************************************************************
Public Sub GetJiraData(Optional ByVal bEndMessage As Boolean = True)
Attribute GetJiraData.VB_ProcData.VB_Invoke_Func = " \n14"
  Const strFilename As String = "\JiraExport.xls"
  Dim wsHome As Worksheet
  Dim strCurlCmd As String


  Application.ScreenUpdating = False
  Application.Calculation = xlCalculationManual

  ' JIRA_Importボタンがあるシート設定
  Set wsHome = Worksheets(gConfigSheetName)
  gSettingFID.OutputPath = ActiveWorkbook.Path & strFilename
  Debug.Print "(GetJiraData):gSettingFID.OutputPath = " & gSettingFID.OutputPath

  ' 基本設定情報を取得
  If (GetBaseInfo() = False) Then
    MsgBox "[強制終了]基本情報(JIRA BASE URL/UserID/Password)を指定して下さい。", vbCritical
    Exit Sub
  End If
  ' FIDを取得
  If (GetModeFID() = False) Then
    MsgBox "[強制終了]Filter IDを指定して下さい。", vbCritical
    Exit Sub
  End If
  ' 指定フィールド情報を取得
  If (GetModeFields() = False) Then
    MsgBox "[強制終了]不正なfields指定です。", vbCritical
    Exit Sub
  End If

  ' Curlコマンド経由でJIRAデータを取得
  Dim ret As Long

  With CreateObject("Wscript.Shell")
    strCurlCmd = MakeCurlCmd(gSettingFID.OutputPath, _
                             gSettingFID.Id, _
                             gSettingFID.FieldsOp)

    ' all
    ret = .Run(strCurlCmd, 7, True)
  End With

  If (ret <> 0) Then
    MsgBox "JIRAデータのダウンロードに失敗しました" & vbCrLf & _
           "(ret :" & ret & ")", vbCritical
    Exit Sub
  End If

  ' Curlで取得したJIRA Data Fileをシートにコピー
  If (CopyJIRADataToSheet(strFilename, gJIRASheetName) = False) Then
    MsgBox "CopyJIRADataToSheet failed!", vbCritical
    Exit Sub
  End If

  Application.Calculation = xlCalculationAutomatic
  Application.ScreenUpdating = True

  ' 元のシートへ戻る
  wsHome.Activate

  If (bEndMessage = True) Then
    MsgBox "処理完了", vbInformation
  End If

End Sub

' 設定パラメータ(基本情報)を取得するFunction
' [引数]
'   なし
'
' [戻り値]
'   Boolean：True  (取得成功)
'          ：False (取得失敗[基本情報の指定無し])
Public Function GetBaseInfo() As Boolean

  With Worksheets(gSettingSheetName)
    gSettingBase.BaseURL = .Range(gBaseURLRangeName).Value
    gSettingBase.UserID = .Range(gUserIDRangeName).Value
    gSettingBase.Password = .Range(gPasswordRangeName).Value
    gSettingBase.CurlPath = .Range(gCurlPathRangeName).Value
  End With

  Debug.Print "(GetBaseInfo):gSettingBase.BaseURL = " & gSettingBase.BaseURL & vbCrLf & _
              "              gSettingBase.UserID = " & gSettingBase.UserID & vbCrLf & _
              "              gSettingBase.Password = " & gSettingBase.Password & vbCrLf & _
              "              gSettingBase.CurlPath = " & gSettingBase.CurlPath

  ' **** 実行条件チェック ****
  ' BaseURL, UserID, Passwordいずれかが指定されていない場合、エラー終了
  If (gSettingBase.BaseURL = "") Or _
     (gSettingBase.UserID = "") Or _
     (gSettingBase.Password = "") Then
    GetBaseInfo = False
    Exit Function
  End If

  GetBaseInfo = True

End Function

' 設定パラメータ(FID情報)を取得するFunction
' [引数]
'   なし
'
' [戻り値]
'   Boolean：True  (取得成功)
'          ：False (取得失敗[FID指定無し])
Public Function GetModeFID() As Boolean

  With Worksheets(gConfigSheetName)
    ' Filter IDを取得
    gSettingFID.Id = .Range(gFIDRangeName).Value
  End With

  Debug.Print "(GetModeFID):gSettingFID.Id = " & gSettingFID.Id

  ' **** 実行条件チェック(FIDが指定されているか) ****
  ' FIDが指定されていない場合、エラー終了
  If (gSettingFID.Id = "") Then
    GetModeFID = False
    Exit Function
  End If

  GetModeFID = True

End Function

' 設定パラメータ(fields情報)を取得するFunction
' [引数]
'   なし
'
' [戻り値]
'   Boolean：True  (取得成功)
'          ：False (取得失敗)
Public Function GetModeFields() As Boolean

  With Worksheets(gConfigSheetName)
    ' Fieldsオプション指定を取得
    If (.CheckBoxes(gCheckBoxFidAll).Value = xlOn) And _
       (.CheckBoxes(gCheckBoxFidCur).Value = xlOff) Then
      gSettingFID.FieldsOp = eFieldsSet.all
    ElseIf (.CheckBoxes(gCheckBoxFidAll).Value = xlOff) And _
           (.CheckBoxes(gCheckBoxFidCur).Value = xlOn) Then
      gSettingFID.FieldsOp = eFieldsSet.Current
    ElseIf (.CheckBoxes(gCheckBoxFidAll).Value = xlOn) And _
           (.CheckBoxes(gCheckBoxFidCur).Value = xlOn) Then
      gSettingFID.FieldsOp = eFieldsSet.Error
    ElseIf (.CheckBoxes(gCheckBoxFidAll).Value = xlOff) And _
           (.CheckBoxes(gCheckBoxFidCur).Value = xlOff) Then
      gSettingFID.FieldsOp = eFieldsSet.Error
    End If
  End With

  Debug.Print "(GetModeFields):gSettingFID.FieldsOp = " & gSettingFID.FieldsOp

  If (gSettingFID.FieldsOp = Error) Then
    GetModeFields = False
    Exit Function
  End If

  GetModeFields = True

End Function

' シート内容を全クリアするFunction
' [引数]
'   strSheetName As String：内容を全クリアするシート名
'
' [戻り値]
'   なし
Private Function ClearSheet(ByVal strSheetName As String)
  Dim ws As Worksheet
  Dim MaxRow As Long
  Dim MaxCol As Long

  Debug.Print "(ClearSheet):strSheetName = " & strSheetName

  ' 同名シートが存在する場合、一旦削除する。
  For Each ws In Worksheets
    If (ws.Name = strSheetName) Then
      Application.DisplayAlerts = False
      Worksheets(strSheetName).Delete
      Application.DisplayAlerts = True
    End If
  Next ws

  ' シートを新規追加
  Worksheets.Add After:=Worksheets(Worksheets.Count)
  ActiveSheet.Name = strSheetName

  ' シート全体の表示形式を文字列に設定
  Cells.NumberFormatLocal = "@"

End Function

' 指定シートのK〜N列の日付を変換するFunction
' [引数]
'   strSheetName As String：日付変換対象シート名
'
' [戻り値]
'   なし
Private Function DateConvSheet(ByVal strSheetName As String)
  Const START_ROW As Long = 2
  Dim wsCur As Worksheet
  Dim vField As Variant
  Dim i As Long
  Dim j As Long
  Dim MaxRow As Long
  Dim strFieldName As String
  Dim rowTemp As Long
  Dim colTemp As Long
  Dim strCellAddr As String

  Debug.Print "(DateConvSheet):strSheetName = " & strSheetName
  Set wsCur = Worksheets(strSheetName)

  wsCur.Activate

  vField = Split(gDateConvField, ",")

  '使用済み最終行を取得
  With wsCur.UsedRange
    MaxRow = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
  End With

  For i = LBound(vField) To UBound(vField)
    Debug.Print "(DateConvSheet):vField(" & i & ") = " & vField(i)
    strFieldName = vField(i)

    ' 日付変更対象フィールド見出しの行/列を取得
    If (GetHeadPos(strSheetName, _
                   strFieldName, _
                   rowTemp, colTemp) = False) Then
      Debug.Print "(DateConvSheet):GetHeadPos Error!" & vbCrLf & _
                  "                strSheetName = " & strSheetName & _
                  " strFieldName = " & strFieldName
      GoTo DATA_CONV_SKIP
    End If

    Debug.Print "(DateConvSheet):rowTemp = " & rowTemp & _
                " colTemp = " & colTemp

    ' 日付の値が入力されているセルのみ、表示形式を日付に設定する
    For j = START_ROW To MaxRow
      strCellAddr = wsCur.Cells(j, colTemp).Address

      With wsCur
        If (.Range(strCellAddr).Value <> "") Then
          .Range(strCellAddr).NumberFormatLocal = "yyyy/m/d"
        End If
      End With
    Next j
DATA_CONV_SKIP:
  Next i

End Function

' JIRAデータファイルをシートにコピーするFunction
' [引数]
'   strJIRADataFile As String：Curlで取得したJIRA Dataファイル名
'   strSheetName As String：JIRA Dataファイル内容のコピー先シート名
'
' [戻り値]
'   Boolean：True  (処理成功)
'          ：False (処理失敗)
Private Function CopyJIRADataToSheet(ByVal strJIRADataFile As String, _
                                     ByVal strSheetName As String) As Boolean
  Dim wb As Workbook
  Dim buf As String
  Dim Target As Workbook
  Dim Original As Workbook
  Dim wsCopyDist As Worksheet
  Dim i As Long
  Dim MaxRow As Long
  Dim MaxCol As Long
  Dim strPath As String
  Const JIRA_START_ROW As Long = 4
  Dim rEmptyRow As Range
  Dim LastRow As Long

  Debug.Print "(CopyJIRADataToSheet):strJIRADataFile = " & strJIRADataFile & _
              " strSheetName = " & strSheetName

  ' 指定シートの内容を全クリア
  Call ClearSheet(strSheetName)

  Set Original = ThisWorkbook
  Set wsCopyDist = Original.Worksheets(strSheetName)

  strPath = ThisWorkbook.Path & "\" & strJIRADataFile
  Debug.Print "(CopyJIRADataToSheet):strPath = " & strPath

  ' ファイルの存在チェック
  buf = Dir(strPath)
  If buf = "" Then
    MsgBox strPath & vbCrLf & _
           "は存在しません", vbExclamation
    
    CopyJIRADataToSheet = False
    Exit Function
  End If

  ' 同名ブックのチェック
  For Each wb In Workbooks
    If wb.Name = buf Then
      MsgBox buf & vbCrLf & "は既に開いています", vbExclamation

      CopyJIRADataToSheet = False
      Exit Function
    End If
  Next wb

  ' Open JIRA Data File
  ' Excel読込中のエラー(セルのデータが大きすぎます)が出る場合の対策。
  Application.DisplayAlerts = False
  Set Target = Workbooks.Open(FileName:=strPath)
  Application.DisplayAlerts = True

  ' 使用済み最終行＆列を取得
  With Target.Sheets(1).UsedRange
    MaxRow = .Find("*", , xlFormulas, , xlByRows, xlPrevious).Row
    MaxCol = .Find("*", , xlFormulas, , xlByColumns, xlPrevious).Column
  End With

  Original.Activate

  For i = JIRA_START_ROW To MaxRow - 1
    ' Copy JIRA Data File to specified sheet
    Target.Sheets(1).Cells(i, 1).Resize(, MaxCol).Copy _
      wsCopyDist.Cells(i - (JIRA_START_ROW - 1), 1)
  Next

  ' 結合セルがあるフィールド列を削除する
  For i = MaxCol To 1 Step -1
    With wsCopyDist.Columns(i)
      If .Rows(1).MergeCells Then
        .Delete
      End If
    End With
  Next i

  ' Close JIRA Data File
  Target.Close SaveChanges:=False

  Call DateConvSheet(strSheetName)

  wsCopyDist.Activate

  ' シート全体の行/列幅を設定
  wsCopyDist.Cells.RowHeight = 12
  wsCopyDist.Cells.ColumnWidth = 15
  ' シート全体のフォント名/サイズを設定
  wsCopyDist.Cells.Font.Name = "Meiryo UI"
  wsCopyDist.Cells.Font.Size = 10
  ' シートの表示倍率を設定
  ActiveWindow.Zoom = 70

  LastRow = Cells(Rows.Count, 1).End(xlUp).Row
  With ActiveSheet
    For i = 2 To LastRow
      ' i行/1列目のセルが空白だった場合
      If IsEmpty(Cells(i, 1).Value) Then
        ' 最初の空白行
        If rEmptyRow Is Nothing Then
          Set rEmptyRow = .Rows(i).EntireRow
        ' 2件目以降の空白行
        Else
          Set rEmptyRow = Union(rEmptyRow, .Rows(i).EntireRow)
        End If
      End If
    Next i
  End With

  ' 空白行があれば一括で削除する
  If Not rEmptyRow Is Nothing Then
        rEmptyRow.Delete
  End If

  CopyJIRADataToSheet = True

End Function

' 指定シートにおいて、指定文字列(見出し)のセル列を特定するFunction
' [引数]
'   strSheetName As String：検索対象シート名
'   strHead As String     ：見出し文字列
'   rowNum As Long        ：行番号を格納する変数
'   colNum As Long        ：列番号を格納する変数
'
' [戻り値]
'   Boolean：True  (指定文字列の見出しセル列取得に成功)
'          ：False (指定文字列の見出しセル列取得に失敗)
Private Function GetHeadPos(ByVal strSheetName As String, _
                            ByVal strHead As String, _
                            ByRef rowNum As Long, _
                            ByRef colNum As Long) As Boolean
  Dim wsRtn As Worksheet
  Dim FoundCell As Range
  Dim i As Long

  Set wsRtn = ActiveSheet
  ' 検索対象シートに切替
  Worksheets(strSheetName).Activate

  Set FoundCell = Cells.Find(What:=strHead, LookAt:=xlWhole)
  If FoundCell Is Nothing Then
    rowNum = 0
    colNum = 0
    Debug.Print "(GetHeadPos):strSheetName = " & strSheetName & _
                " strHead = " & strHead & " rowNum = " & rowNum & _
                " colNum = " & colNum
    GetHeadPos = False
    ' 元のシートに切替
    wsRtn.Activate
    Exit Function
  End If

  rowNum = FoundCell.Row
  colNum = FoundCell.Column

  ' 元のシートに切替
  wsRtn.Activate

  GetHeadPos = True

End Function

' 指定したパラメータを基にCurlコマンドを生成するFunction
' [引数]
'   strPath As String     ：Curlで取得したデータの保存先パス
'   strFID As String      ：Curlで取得する際の対象FID
'   FieldsOp As eFieldsSet：Curlで取得する際の対象フィールド
'
' [戻り値]
'   String：生成したCurlコマンド文字列
Private Function MakeCurlCmd(ByVal strPath As String, _
                             ByVal strFID As String, _
                             ByVal FieldsOp As eFieldsSet) As String
  Dim strCmd As String
  Dim strCurl As String

  If (gSettingBase.CurlPath = "") Then
    strCurl = "curl"
  Else
    strCurl = gSettingBase.CurlPath & "\curl"
  End If

  Select Case FieldsOp
    Case eFieldsSet.all
      strCmd = strCurl & " -Lkv -o """ & strPath & _
               """ -v -H ""Content-Type: application/json"" -X GET -u " & gSettingBase.UserID & ":" & _
               gSettingBase.Password & " " & _
               "" & gSettingBase.BaseURL & "/sr/jira.issueviews:searchrequest-html-all-fields/" & _
               strFID & "/SearchRequest-" & strFID & ".html?"""
    Case eFieldsSet.Current
      strCmd = strCurl & " -Lkv -o """ & strPath & _
               """ -v -H ""Content-Type: application/json"" -X GET -u " & gSettingBase.UserID & ":" & _
               gSettingBase.Password & " " & _
               "" & gSettingBase.BaseURL & "/sr/jira.issueviews:searchrequest-html-current-fields/" & _
               strFID & "/SearchRequest-" & strFID & ".html?"""
    Case Else
      strCmd = strCurl & " -Lkv -o """ & strPath & _
               """ -v -H ""Content-Type: application/json"" -X GET -u " & gSettingBase.UserID & ":" & _
               gSettingBase.Password & " " & _
               "" & gSettingBase.BaseURL & "/sr/jira.issueviews:searchrequest-html-all-fields/" & _
               strFID & "/SearchRequest-" & strFID & ".html?"""
  End Select

  Debug.Print "(MakeCurlCmd):strCmd = " & vbCrLf & strCmd
  MakeCurlCmd = strCmd

End Function

'*******************************************************************************
' JIRAデータ取得用のオプション選択時のイベント処理
'
' [引数]
' なし
' [戻り値]
' なし
'
' [処理概要]
' (1)該当チェックボックスがONされた際、ペアとなるチェックボックスをOFFにする。
'    該当チェックボックスがOFFされた際、ペアとなるチェックボックスをONにする。
'*******************************************************************************
Private Sub CheckBox_FID_All_Change()
  With ActiveSheet
    If (.CheckBoxes(gCheckBoxFidAll).Value = xlOn) Then
      .CheckBoxes(gCheckBoxFidCur).Value = xlOff
    Else
      .CheckBoxes(gCheckBoxFidCur).Value = xlOn
    End If
  End With
End Sub

Private Sub CheckBox_FID_Cur_Change()
  With ActiveSheet
    If (.CheckBoxes(gCheckBoxFidCur).Value = xlOn) Then
      .CheckBoxes(gCheckBoxFidAll).Value = xlOff
    Else
      .CheckBoxes(gCheckBoxFidAll).Value = xlOn
    End If
  End With
End Sub

'*******************************************************************************
' JIRA_Import機能を他BookへImportするマクロ
'
' [引数]
' なし
'
' [戻り値]
' なし
'
' [処理概要]
' (Step 1)Import先のBookをファイル選択ウィンドウで指定させる。
' (Step 2)本モジュールを一旦Exportする。
' (Step 3)Exportしたモジュールを(Step 1)で選択されたBookにImportする。
' (Step 4)JIRA_Import/Settingシートをコピーする。
' (Step 5)フォームボタンに登録されているマクロを自Bookのマクロに変更する。
' (Step 6)チェックボックスに登録されているマクロを自Bookのマクロに変更する。
' (Step 7)Save後、ブックを閉じる。
' (Step 8)Exportしたモジュールを削除する。
'*******************************************************************************
Public Sub FuncImport2OtherBook()
  Dim wbTarget As Workbook
  Dim OpenFileName As String
  Dim FileName As String
  Dim vModName As Variant
  Dim strModName As String


  Application.ScreenUpdating = False
  Application.Calculation = xlCalculationManual

  ' (Step 1)Import先のBookをファイル選択ウィンドウで指定させる。
  OpenFileName = Application.GetOpenFilename("Macro Book,*.xlsm?")
  If OpenFileName <> "False" Then
    FileName = Dir(OpenFileName)
  Else
    MsgBox "キャンセルされました", vbInformation
    Exit Sub
  End If

  Debug.Print "(FuncImport2OtherBook):OpenFileName = " & OpenFileName & _
              " FileName = " & FileName

  ' (Step 2)本モジュールを一旦Exportする。
  vModName = Application.VBE.ActiveCodePane.CodeModule
  strModName = vModName & ".bas"
  With ThisWorkbook.VBProject.VBComponents(vModName)
    .Export strModName
  End With

  ' (Step 3)Exportしたモジュールを(Step 1)で選択されたBookにImportする。
  Workbooks.Open FileName:=OpenFileName
  Set wbTarget = ActiveWorkbook
  wbTarget.VBProject.VBComponents.Import strModName

  ' (Step 4)JIRA_Import/Settingシートをコピーする。
  ThisWorkbook.Worksheets(gConfigSheetName).Copy _
    After:=wbTarget.Worksheets(wbTarget.Worksheets.Count)
  ThisWorkbook.Worksheets(gSettingSheetName).Copy _
    After:=wbTarget.Worksheets(wbTarget.Worksheets.Count)

  ' (Step 5)フォームボタンに登録されているマクロを自Bookのマクロに変更する。
  wbTarget.Sheets(gConfigSheetName).Shapes(gJiraImportBtn).OnAction = _
    wbTarget.Name & "!GetJiraData"
  wbTarget.Sheets(gConfigSheetName).Shapes(gFuncImportBtn).OnAction = _
    wbTarget.Name & "!FuncImport2OtherBook"

  ' (Step 6)チェックボックスに登録されているマクロを自Bookのマクロに変更する。
  wbTarget.Sheets(gConfigSheetName).CheckBoxes(gCheckBoxFidAll).OnAction = _
    wbTarget.Name & "!CheckBox_FID_All_Change"
  wbTarget.Sheets(gConfigSheetName).CheckBoxes(gCheckBoxFidCur).OnAction = _
    wbTarget.Name & "!CheckBox_FID_Cur_Change"

  ' (Step 7)Save後、ブックを閉じて終了。
  wbTarget.Save
  wbTarget.Close

  ' (Step 8)Exportしたモジュールを削除する。
  Kill strModName

  Application.Calculation = xlCalculationAutomatic
  Application.ScreenUpdating = True

  MsgBox "処理完了", vbInformation

End Sub
