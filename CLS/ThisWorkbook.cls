VERSION 1.0 CLASS
BEGIN
  MultiUse = -1  'True
END
Attribute VB_Name = "ThisWorkbook"
Attribute VB_GlobalNameSpace = False
Attribute VB_Creatable = False
Attribute VB_PredeclaredId = True
Attribute VB_Exposed = True

' ワークブックを開く時のイベント
Private Sub マクロ更新()

 
    Dim rc As Integer
    
    rc = MsgBox("マクロエクスポートを実施しますか？", vbYesNo + vbQuestion, "確認")
    If rc = vbYes Then
        '
        Call ExportModules
        
        rc = 0

        rc = MsgBox("マクロを再取り込みしますか？", vbYesNo + vbQuestion, "確認")
        
        If rc = vbYes Then
            ' txtに書いてある外部ライブラリを読み込み
            load_from_conf ".\libdef.txt"
        Else
            MsgBox "再取り込み処理をスキップします"
        End If
            
    Else
        MsgBox "エクスポート処理をスキップします"
    End If


       
End Sub

' -------------------- モジュール読み込みに関する関数 --------------------
' 設定ファイルに書いてある外部ライブラリを読み込みます。
Sub load_from_conf(conf_path)
    
    Dim rc As Integer
    
    rc = MsgBox("マクロ削除しますか？", vbYesNo + vbQuestion, "確認")
    If rc = vbYes Then
        ' 全モジュールを削除
        clear_modules
    Else
        MsgBox "削除処理をスキップします"
    End If
    
    ' 絶対パスに変換
    conf_path = abs_path(conf_path)
    If Dir(conf_path) = "" Then
        MsgBox "外部ライブラリ定義" & conf_path & "が存在しません。"
        Exit Sub
    End If
    
    ' 読み取り
    fp = FreeFile
    Open conf_path For Input As #fp
    Do Until EOF(fp)
        ' １行ずつ
        Line Input #fp, temp_str
        If Len(temp_str) > 0 Then
            module_path = abs_path(temp_str)
            If Dir(module_path) = "" Then
                ' エラー
                MsgBox "モジュール" & module_path & "は存在しません。"
                Exit Do
            Else
                'MsgBox module_path
                ' モジュールとして取り込み
                include module_path
            End If
        End If
    Loop
    Close #fp

    ThisWorkbook.Save
    
End Sub


' あるモジュールを外部から読み込みます。
' パスが.で始まる場合は，相対パスと解釈されます。
Sub include(file_path)
    
    ' 絶対パスに変換
    file_path = abs_path(file_path)
    
    ' 標準モジュールとして登録
    ThisWorkbook.VBProject.VBComponents.Import file_path
End Sub


' 全モジュールを初期化します。
Private Sub clear_modules()
    For Each Component In ThisWorkbook.VBProject.VBComponents
        If Component.Type = 1 Then
            ' この標準モジュールを削除
            ThisWorkbook.VBProject.VBComponents.Remove Component
        End If
    Next Component
End Sub


' ファイルパスを絶対パスに変換します。
Function abs_path(file_path)

    ' 絶対パスに変換
    If Left(file_path, 1) = "." Then
        file_path = ThisWorkbook.Path & Mid(file_path, 2, Len(file_path) - 1)
    End If
    
    abs_path = file_path

End Function


Public Sub ExportModules()
    '現在のワークブックのモジュールをエクスポートする
    Dim targetModule As VBComponent
    Dim outputPath As String
    Dim fileExt As String
    outputPath = ActiveWorkbook.Path & "\backup"
    MsgBox outputPath
    For Each targetModule In ActiveWorkbook.VBProject.VBComponents
        fileExt = GetExtFromModuleType(targetModule.Type)
        If fileExt <> "" Then
            ExportModuleWithExt targetModule, outputPath, fileExt
            Debug.Print "Save " & targetModule.Name
        End If
    Next
End Sub

Private Function GetExtFromModuleType(aType As Integer) As String
    '指定されたモジュール・タイプに対応する拡張子を返す
    Select Case aType
    Case vbext_ct_StdModule
        GetExtFromModuleType = "bas"
    'Case vbext_ct_ClassModule, vbext_ct_Document
        'GetExtFromModuleType = "cls"
    'Case vbext_ct_MSForm
        'GetExtFromModuleType = "frm"
    End Select
End Function

Private Sub ExportModuleWithExt(aModule As VBComponent, Path As String, Ext As String)
    '指定されたモジュールをエクスポートする
    Dim filePath As String
    filePath = Path & "\" & aModule.Name & "." & Ext
    aModule.Export filePath
End Sub



Private Sub Workbook_Open()

End Sub
